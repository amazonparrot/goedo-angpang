﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LANG {
    KOREAN, ENGLISH, JAPANESE
}

public class ScriptLanguageMgr : MonoBehaviour {

    public LANG CurrentLanguage = LANG.KOREAN;

    private LoadCSV _csv;
    private bool isInited;


    // Use this for initialization
    void Start () {
        if(CurrentLanguage != LANG.KOREAN)
	        _csv = GetComponent<LoadCSV>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!isInited && _csv != null && _csv.isLoaded) {
            isInited = true;

        }
    }

    public string Find(string script, int col = -1) {

        if (!isInited) return script;

        if (string.IsNullOrEmpty(script)) {
            return script;
        }

        DataTable table = _csv.table;

        int lang = (int)(CurrentLanguage) + 3;

        if (col == -1) {
            for (int i = 1; i < table.line.Count; i++) {

                string index_str = table.Get(3, i);
                string result = table.Get(lang, i);
                if (result == null) continue;

                if (script.Replace(" ", "").Trim() == index_str.Replace(" ", "").Trim()) {
                    return result;
                }


            }
        }
        else {
            string result = table.Get(lang, col);
            if (result == null) return script;

            return result;
        }



        return script;

    }

    public bool isInit() {
        return isInited;
    }
}
