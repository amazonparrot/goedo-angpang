﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LobyStageInfoBt : MonoBehaviour {

    private DataMgr dataMgr;
    private UILabel StageId;
    private UISprite sprite;
    private GameObject stageLock;
    private UISprite stageLockSprite;
    private int index;
    private bool isUnlocked;


    void Awake() {
        for (int i = 0; i < transform.childCount; i++) {
            GameObject tmp = transform.GetChild(i).gameObject;

            if (tmp.transform.name == "stage_id") {
                StageId = tmp.GetComponent<UILabel>();
                continue;
            }

            if (tmp.transform.name == "Background") {
                sprite = tmp.GetComponent<UISprite>();
                continue;
            }

            if (tmp.transform.name == "stage_lock") {
                stageLock = tmp;
                stageLockSprite = tmp.GetComponent<UISprite>();
                continue;
            }
        }
    }

    // Use this for initialization
    void Start () {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        isUnlocked =
            dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_UNLOCK[
                index];

        stageLock.SetActive(isUnlocked);
    }
	
	// Update is called once per frame
	void Update () {
        StageId.text = "" + (15 * dataMgr.dataClass.CURRENT_EPISODE_ID + index + 1);
        isUnlocked =
            dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_UNLOCK[
                index];

        stageLock.SetActive(!isUnlocked);

    }



    public void SetAlpha(float value) {
        sprite.alpha = value;
        stageLockSprite.alpha = value;
        if (!isUnlocked) {
            StageId.alpha = 0;
        }
        else {
            StageId.alpha = value;
        }
    }

    public void SetIndex(int i) {
        index = i;
    }
}
