﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyStageScoreUI : MonoBehaviour {

    private GameObject star1;
    private GameObject star2;
    private GameObject star3;

    private DataMgr dataMgr;

	// Use this for initialization
	void Start () {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();

        star1 = GameObject.Find("clear_grade_1");
        star2 = GameObject.Find("clear_grade_2");
        star3 = GameObject.Find("clear_grade_3");

        GetScore();
    }

    // Update is called once per frame
    void Update () {
            GetScore();
	}

    public void GetScore() {
        int score = dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SCORE[dataMgr.dataClass.CURRENT_STAGE_ID];


        switch (score) {
            case 3:
                star3.SetActive(true);
                star2.SetActive(true);
                star1.SetActive(true);
                break;
            case 2:
                star3.SetActive(false);
                star2.SetActive(true);
                star1.SetActive(true);
                break;
            case 1:
                star3.SetActive(false);
                star2.SetActive(false);
                star1.SetActive(true);
                break;
            default:
                star3.SetActive(false);
                star2.SetActive(false);
                star1.SetActive(false);
                break;
        }
    }
}
