﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyInfoBt : MonoBehaviour {

    public GameObject info_panel;
    private LobyPopInfoUI lobyPopInfoUi;

	// Use this for initialization
	void Start () {
	    lobyPopInfoUi = info_panel.GetComponent<LobyPopInfoUI>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnClick() {
        lobyPopInfoUi.SetActivePanel(true);
    }
}
