﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyStageBts : MonoBehaviour {

    public GameObject bt_stage_info;
    public float Sensitivity = 1;

    private GameObject[] stage_info_bts;
    private DataMgr dataMgr;

    private float mouseStartPositionX;
    private Vector3 deltaPosition;
    private Vector3 targetPosition;

    private bool isMouseUp;

    public int index;
    public bool isRelax;

	// Use this for initialization
	void Start () {

        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        bt_stage_info.transform.localPosition = -Vector3.one * 9999;

	    int currentStageSize = dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SIZE;
        stage_info_bts = new GameObject[currentStageSize];


	    for (int i = 0; i < stage_info_bts.Length; i++) {
	        GameObject _object = Instantiate(bt_stage_info, transform);
	        stage_info_bts[i] = _object;
            _object.GetComponent<LobyStageInfoBt>().SetIndex(i);

            _object.transform.localPosition = new Vector3(i * 90, 0, 0);

            for (int j = 0; j < _object.transform.childCount; j++) {
                GameObject tmp = _object.transform.GetChild(j).gameObject;

                if (tmp.transform.name == "stage_id") {
                    tmp.GetComponent<UILabel>().text = "" + (15 * dataMgr.dataClass.CURRENT_EPISODE_ID + i + 1);
                    break;
                }
            }
        }
	    index = -dataMgr.dataClass.CURRENT_STAGE_ID;

        targetPosition = transform.localPosition = new Vector3(index*90, -8, 0);
	    isRelax = true;

	}
	
	// Update is called once per frame
	void Update () {

	    if (Input.GetMouseButtonDown(0)) {
	        mouseStartPositionX = Input.mousePosition.x;
	        deltaPosition = transform.localPosition;
	        isMouseUp = false;
	    }

	    if (Input.GetMouseButton(0)) {
	        if (isRelax && Mathf.Abs(mouseStartPositionX - Input.mousePosition.x) > 15) {
	            isRelax = false;
	        }

            if(!isRelax)
	            transform.localPosition = deltaPosition + new Vector3(Input.mousePosition.x * Sensitivity - mouseStartPositionX, 0, 0);

            index = (int)Mathf.Round(transform.localPosition.x / 90);

            if (index > 0) {
                index = 0;
            }
            else if (index <= -stage_info_bts.Length) {
                index = -stage_info_bts.Length + 1;
            }

            dataMgr.dataClass.CURRENT_STAGE_ID = -index;
        }

	    if (Input.GetMouseButtonUp(0)) {
	        isMouseUp = true;

	        if (!isRelax) {
	            isRelax = true;
                index = (int)Mathf.Round(transform.localPosition.x / 90);

                if (index > 0) {
                    index = 0;
                }
                else if (index <= -stage_info_bts.Length) {
                    index = -stage_info_bts.Length + 1;
                }
                dataMgr.dataClass.CURRENT_STAGE_ID = -index;
                targetPosition = new Vector3(index * 90, -8, 0);
                dataMgr.SaveData();
            }

        }

	    if (isMouseUp) {
	        if (dataMgr.dataClass.CURRENT_STAGE_ID != -index) {
                index = -dataMgr.dataClass.CURRENT_STAGE_ID;
                targetPosition = new Vector3(index * 90, -8, 0);
            }
	        transform.localPosition = Vector3.Lerp(transform.localPosition, targetPosition, 0.5f);

	    }
        

        for (int i = 0; i < stage_info_bts.Length; i++) {
            GameObject _object = stage_info_bts[i];
            LobyStageInfoBt bt = _object.GetComponent<LobyStageInfoBt>();
            Vector3 objPos = _object.transform.localPosition + transform.localPosition;

            float objScaleValue = 1/(Mathf.Abs(objPos.x/200)+1);
            float alpha = 1 - Mathf.Abs(objPos.x/200);
            alpha = alpha < 0 ? 0 : alpha;
            bt.SetAlpha(alpha);

            _object.transform.localScale = new Vector3(objScaleValue, objScaleValue, 1);
        }

    }
}
