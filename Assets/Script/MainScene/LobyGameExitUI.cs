﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyGameExitUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Application.platform == RuntimePlatform.Android) {

            if (Input.GetKeyUp(KeyCode.Escape)) {

                if (!GameObject.Find("pop_lotto_panel").GetComponent<LottoPanel>().isLottoActived) {
                    Application.Quit();
                }

            }

        }
    }
}
