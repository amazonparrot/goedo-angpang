﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyPopInfoUI : MonoBehaviour {

    public GameObject VersionLabel;
    public GameObject CloseBt;
    private UILabel label;
    private PanelCloseBt panelCloseBt;

    private UIPanel _uiPanel;

	// Use this for initialization
	void Start () {
	    label = VersionLabel.GetComponent<UILabel>();
	    label.text = Application.version;
	    panelCloseBt = CloseBt.GetComponent<PanelCloseBt>();
	    _uiPanel = GetComponent<UIPanel>();
	    _uiPanel.alpha = 0;

	    panelCloseBt.isClose = true;
	}
	
	// Update is called once per frame
	void Update () {
	    if (!panelCloseBt.isClose) {

            //if(!gameObject.activeInHierarchy)
	           // gameObject.SetActive(true);

	        if (_uiPanel.alpha < 0.99f)
	            _uiPanel.alpha = Mathf.Lerp(_uiPanel.alpha, 1, 0.1f);
	        else {
	            _uiPanel.alpha = 1;
	        }
	    }
	    else {
	        //if (gameObject.activeInHierarchy && _uiPanel.alpha <= 0) {
	        //    gameObject.SetActive(false);
	        //}
	    }
	}

    public void SetActivePanel(bool active) {
        panelCloseBt.isClose = !active;
    }
}
