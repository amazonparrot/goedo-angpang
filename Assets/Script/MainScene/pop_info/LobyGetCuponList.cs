﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


public enum CuponKind { Ticket, Skin };

public class Cupon {
    public string code;
    public CuponKind kind;
    public string received;
}

public class LobyGetCuponList : MonoBehaviour {

    public bool isAnalyzed;
    public bool isUsed;
    public string ResultLog;

    private List<Cupon> cupons;
    private DataMgr dataMgr;

    private string Code;

    // Use this for initialization
    void Start () {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        isAnalyzed = true;
        isUsed = true;
    }

    // Update is called once per frame
    void Update () {
        if (isAnalyzed && !isUsed) {

            isUsed = true;

            for (int i = 0; i < cupons.Count; i++) {
                var cupon = cupons[i];

                if (cupon.code.ToLower() == Code) {

                    for (int j = 0; j < dataMgr.dataClass.CUPON_USED_LIST.Count; j++) {
                        if (dataMgr.dataClass.CUPON_USED_LIST[j] == Code) {
                            ResultLog = "이미 사용한 쿠폰입니다.";
                            Debug.Log(Code + " is already used");
                            dataMgr.SetLoading(false);
                            return;
                        }
                    }

                    switch (cupon.kind) {
                        case CuponKind.Ticket:
                            dataMgr.dataClass.CURRENT_LOTTO_TICKET += int.Parse(cupon.received);
                            break;
                        case CuponKind.Skin:
                            dataMgr.dataClass.SKIN_UNLOCKED_LIST.Add(cupon.received);
                            break;
                    }

                    dataMgr.SaveData();
                    ResultLog = "쿠폰 지급이 완료되었습니다.";
                }
            }

            if (ResultLog == "") {
                ResultLog = "없는 쿠폰이거나 접속오류입니다.";
            }
            else {
                dataMgr.dataClass.CUPON_USED_LIST.Add(Code);
                dataMgr.SaveData2();
            }
            dataMgr.SetLoading(false);

        }
    }

    private IEnumerator AnalyzingCupon(string cuponCode) {
        if (isAnalyzed) yield return null;
        WWW CuponList = new WWW("https://bitbucket.org/amazonparrot/goedo-angpang/wiki/CuponList");
        yield return CuponList;

        byte[] bConvert = Encoding.Convert(Encoding.UTF8, Encoding.Default, CuponList.bytes);

        // utf-8 인코딩
        
        string encodedString = System.Text.Encoding.Default.GetString(bConvert);
        string source;
        string[] sources = encodedString.Split('$');

        cuponCode = cuponCode.ToLower();

        cupons = new List<Cupon>();

        for (int index = 0; index < sources.Length; index++) {
            string tmp = sources[index];

            if (tmp.StartsWith("code")) {
                Debug.Log(tmp);

                string[] splitedStr = tmp.Split(',');

                Cupon cupon = new Cupon();

                for (int i = 0; i < splitedStr.Length; i++) {
                    string sp_tmp = splitedStr[i];

                    if (sp_tmp.StartsWith("code=")) {
                        cupon.code = sp_tmp.Substring(5);
                        Debug.Log("code : " + cupon.code);
                    }
                    else if (sp_tmp.StartsWith("receive=")) {

                        string kind = sp_tmp.Substring(8).Split(':')[0];
                        Debug.Log("kind : " + kind);

                        switch (kind) {
                            case "ticket":
                                cupon.kind = CuponKind.Ticket;
                                Debug.Log("ticket");
                                break;
                            case "skin":
                                cupon.kind = CuponKind.Skin;
                                Debug.Log("skin");
                                break;
                        }

                        cupon.received = sp_tmp.Split(':')[1];
                        Debug.Log("received : " + cupon.received);

                    }

                }

                cupons.Add(cupon);

            }
        }

        isAnalyzed = true;
    }

    public void GetCupon(string cuponCode) {
        Debug.Log("Start");
        dataMgr.SetLoading(true);
        isAnalyzed = false;
        isUsed = false;
        ResultLog = "";
        Code = cuponCode;
        StartCoroutine(AnalyzingCupon(cuponCode));
    }
}
