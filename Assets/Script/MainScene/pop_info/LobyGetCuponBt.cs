﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyGetCuponBt : MonoBehaviour {

    public GameObject InputLabel;
    private UIInput input;
    private LobyGetCuponList CuponList;

    private bool isResut;

	// Use this for initialization
	void Start () {
	    input = InputLabel.GetComponent<UIInput>();
	    CuponList = GetComponent<LobyGetCuponList>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (CuponList.isAnalyzed && CuponList.isUsed && !isResut) {
	        isResut = true;
	        if (CuponList.ResultLog == "") {
	            input.label.text = "쿠폰코드";  
	        }else
	            input.label.text = CuponList.ResultLog;
	    }
	}

    void OnClick() {
        if (!CuponList.isAnalyzed) return;
        isResut = false;
        CuponList.GetCupon(input.text);
    }
}
