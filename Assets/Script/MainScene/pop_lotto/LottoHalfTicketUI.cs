﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LottoHalfTicketUI : MonoBehaviour {

    public GameObject Gauge;

    private LottoPanel lottoPanel;
    private DataMgr dataMgr;

    private UISprite sprite;

    private bool _isActived;

	// Use this for initialization
	void Start () {
	    lottoPanel = GameObject.Find("pop_lotto_panel").GetComponent<LottoPanel>();

        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
	    sprite = Gauge.GetComponent<UISprite>();
        
    }

    // Update is called once per frame
    void Update () {

        if (lottoPanel.isLottoActived) {
            _isActived = true;
            float gauge_value = dataMgr.dataClass.CURRENT_LOTTO_HALF_TICKET;

            if (gauge_value <= 0) {
                sprite.spriteName = "half_ticket1";
            }else if (gauge_value >= 0.6f) {
                sprite.spriteName = "half_ticket3";
            }
            else if(gauge_value >= 0.3f){
                sprite.spriteName = "half_ticket2";
            }
        }
        else if(_isActived){
            _isActived = false;
        }
	}
}
