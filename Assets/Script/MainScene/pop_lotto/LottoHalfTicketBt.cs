﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LottoHalfTicketBt : MonoBehaviour {

	public GameObject bubble_info;
    private DataMgr dataMgr;

	private TweenScale bubbleScale;
	private float deltaTime;
	private bool isClicked;

	// Use this for initialization
	void Start () {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
		bubbleScale = bubble_info.GetComponent<TweenScale> ();
    }

    // Update is called once per frame
    void Update () {
		if (isClicked) {
			deltaTime += Time.deltaTime;

			if (deltaTime > 4) {
				isClicked = false;
				bubbleScale.enabled = true;
				bubbleScale.from = Vector3.one;
				bubbleScale.to = Vector3.zero;
				bubbleScale.Reset ();
			}

		}
	}

    void OnClick() {

		deltaTime = 0;
		isClicked = true;

		bubbleScale.enabled = true;
		bubbleScale.from = Vector3.zero;
		bubbleScale.to = Vector3.one;
		bubbleScale.Reset ();


    }
}
