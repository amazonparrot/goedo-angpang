﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LottoBlingAnimation : MonoBehaviour {

    private SpriteRenderer renderer;
    private DataMgr dataMgr;
    private UIPanel lottoPanel;

	// Use this for initialization
	void Start () {
	    dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
	    renderer = GetComponent<SpriteRenderer>();
	    lottoPanel = GameObject.Find("pop_lotto_panel").GetComponent<UIPanel>();
	}
	
	// Update is called once per frame
	void Update () {

        if (dataMgr.dataClass.CURRENT_LOTTO_SPECIAL_TICKET > 0) {
	        renderer.color = new Color(1, 1, 1, lottoPanel.alpha);

	    }else if (renderer.color.a > 0) {
	        renderer.color = new Color(1, 1, 1, 0);

	    }


	}
}
