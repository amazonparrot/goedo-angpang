﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LottoBt : MonoBehaviour {

    private LottoPanel lottoPanel;


    // Use this for initialization
    void Start () {
        lottoPanel = GameObject.Find("pop_lotto_panel").GetComponent<LottoPanel>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnClick() {
        lottoPanel.SetLottoActive(true);
    }
}
