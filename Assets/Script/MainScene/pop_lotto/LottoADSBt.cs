﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ANDROID
using UnityEngine.Advertisements;
#endif
using Random = UnityEngine.Random;

public class LottoADSBt : MonoBehaviour {

    public int ADS_Count = 3;
    
    private DataMgr dataMgr;
    private LottoPanel lottoPanel;

    // Use this for initialization
    void Start () {

        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        lottoPanel = GameObject.Find("pop_lotto_panel").GetComponent<LottoPanel>();

#if UNITY_ANDROID

        if (Advertisement.isSupported) {
            Advertisement.Initialize(dataMgr.AndroidGameId, true);
        }

#endif

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnClick() {
        float currentDate = DateTime.Now.Year * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day;

        if (dataMgr.dataClass.CURRENT_LOTTO_AD_COUNT >= ADS_Count) {
            if (dataMgr.dataClass.CURRENT_LOTTO_AD_TIME < currentDate) {
                dataMgr.dataClass.CURRENT_LOTTO_AD_COUNT = 0;
                dataMgr.dataClass.CURRENT_LOTTO_AD_TIME = currentDate;
            }
            else {
                return;
            }
            
        }

        dataMgr.dataClass.CURRENT_LOTTO_AD_TIME = currentDate;

        ShowRewardedVideo();

    }

    void ShowRewardedVideo() {
#if UNITY_ANDROID
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show("rewardedVideo", options);
#endif
    }

#if UNITY_ANDROID

    void HandleShowResult(ShowResult result) {
        if (result == ShowResult.Finished) {
            dataMgr.dataClass.CURRENT_LOTTO_AD_COUNT++;
            dataMgr.SaveData();
            SetLotto();
        }
        else if (result == ShowResult.Skipped) {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");

        }
        else if (result == ShowResult.Failed) {
            Debug.LogWarning("Video failed to show");
        }
    }

#endif

    void SetLotto() {
        Mesh mesh;
        Texture2D texture;

        int a = Random.Range(1, dataMgr.GetComponent<ClosetList>().ClosetCategory.Length);
        int b = Random.Range(0, 8);
        float c = Random.Range(0f, 100f);

        //special
        if (c <= 1f) {
            a = 0;
            while (b == 3) {
                b = Random.Range(0, 8);
            }
        }


        Texture2D[] textures;
        textures =
            Resources.LoadAll<Texture2D>("pcskin/" + dataMgr.GetComponent<ClosetList>().ClosetCategoryPath[a] +
                                         "/map");


        texture = textures[b];
        mesh =
            Resources.Load<Mesh>("pcskin/" + dataMgr.GetComponent<ClosetList>().ClosetCategoryPath[a] + "/fbx/" +
                                 texture.name);


        bool isOverlap = false;

        for (int j = 0; j < dataMgr.dataClass.SKIN_UNLOCKED_LIST.Count; j++) {
            string tmp = dataMgr.dataClass.SKIN_UNLOCKED_LIST[j];

            if (tmp == texture.name) {
                isOverlap = true;
                break;
            }
        }

        if (!isOverlap)
            dataMgr.dataClass.SKIN_UNLOCKED_LIST.Add(texture.name);
        
        dataMgr.SaveData();

        lottoPanel.isSpecial = a == 0;
        lottoPanel.UseLotto(mesh, texture);
        lottoPanel.isOverlap = isOverlap;
    }

}
