﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LottoUseTicketBt : MonoBehaviour {

    private LottoPanel lottoPanel;
    private DataMgr dataMgr;
    private ScriptLanguageMgr languageMgr;

    private UILabel label;
    private UISprite sprite;

    private bool isSpecial;

    // Use this for initialization
    void Start() {
        lottoPanel = GameObject.Find("pop_lotto_panel").GetComponent<LottoPanel>();
        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        languageMgr = GameObject.Find("DataManager").GetComponent<ScriptLanguageMgr>();

        isSpecial = dataMgr.dataClass.CURRENT_LOTTO_SPECIAL_TICKET > 0;

        for (var i = 0; i < transform.childCount; i++) {
            var tmp = transform.GetChild(i).gameObject;

            if (tmp.transform.name == "lotto_count_label") {
                label = tmp.GetComponent<UILabel>();
                continue;
            }

            if (tmp.transform.name == "Background") {
                sprite = tmp.GetComponent<UISprite>();
            }

        }
    }

    // Update is called once per frame
    void Update () {
        if(languageMgr.CurrentLanguage == LANG.JAPANESE){
            label.text = "チケット : ";
        }else if(languageMgr.CurrentLanguage == LANG.ENGLISH){
            label.text = "Tickets : ";
        }else{
            label.text = "뽑기권 : ";
        }
        label.text += dataMgr.dataClass.CURRENT_LOTTO_TICKET;
        isSpecial = dataMgr.dataClass.CURRENT_LOTTO_SPECIAL_TICKET > 0;

        if (isSpecial && sprite.spriteName != "lotto_chance_special") {
            sprite.spriteName = "lotto_chance_special";
        }
        else if(!isSpecial && sprite.spriteName != "lotto_chance") {
            sprite.spriteName = "lotto_chance";

        }

    }

    void OnClick() {
        ALCKDNWKSLLDFJKWNLSCJESL();
    }

    private void ALCKDNEKSLLDFJKWNLSCJESL() {
        
    }

    private void ALCKDNWKSLLDFJKWNLSCJESL() {
        int i = dataMgr.dataClass.CURRENT_LOTTO_TICKET - 1;

        if (i < 0) {    //살래요? 로 변환필요
            return;
        }


        Mesh mesh;
        Texture2D texture;

        int a = Random.Range(1, dataMgr.GetComponent<ClosetList>().ClosetCategory.Length);
        int b = Random.Range(0, 8);
        float c = Random.Range(0f, 100f);

        //special
        if (c <= 1f) {
            a = 0;
            while (b == 3 || b == 0) {
                b = Random.Range(0, 8);
            }
        }

        if (isSpecial) {
            dataMgr.dataClass.CURRENT_LOTTO_SPECIAL_TICKET--;
            isSpecial = dataMgr.dataClass.CURRENT_LOTTO_SPECIAL_TICKET > 0;
            dataMgr.SaveData();
            a = 0;
            while (b == 3 || b == 0) {
                b = Random.Range(0, 8);
            }
        }

        Texture2D[] textures;
        textures =
            Resources.LoadAll<Texture2D>("pcskin/" + dataMgr.GetComponent<ClosetList>().ClosetCategoryPath[a] +
                                         "/map");


        texture = textures[b];
        mesh =
            Resources.Load<Mesh>("pcskin/" + dataMgr.GetComponent<ClosetList>().ClosetCategoryPath[a] + "/fbx/" +
                                 texture.name);


        bool isOverlap = false;

        for (int j = 0; j < dataMgr.dataClass.SKIN_UNLOCKED_LIST.Count; j++) {
            string tmp = dataMgr.dataClass.SKIN_UNLOCKED_LIST[j];

            if (tmp == texture.name) {
                isOverlap = true;
                break;
            }
        }

        if (!isOverlap)
            dataMgr.dataClass.SKIN_UNLOCKED_LIST.Add(texture.name);

        dataMgr.dataClass.CURRENT_LOTTO_TICKET = i;
        dataMgr.SaveData();

        lottoPanel.isSpecial = a == 0;
        lottoPanel.UseLotto(mesh, texture);
        lottoPanel.isOverlap = isOverlap;

    }

    private void ALCKDNEKSLLDFJKWNL2CJESL() {
        
    }

}
