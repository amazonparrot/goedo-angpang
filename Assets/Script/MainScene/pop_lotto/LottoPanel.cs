﻿using UnityEngine;

public class LottoPanel : MonoBehaviour {
    private DataMgr dataMgr;
    private BGMMgr bgmMgr;
    private float flashDeltaTime;
    public GameObject flashObject;
    public GameObject flashTouchGameObject;

    public GameObject GetTicketPopUp;

    private TweenAlpha flashSprite;
    private UISprite touchSprite;

    public bool isLottoActived;
    public bool isLottoUsed;
    public bool isOverlap;
    public bool isSpecial;
    public GameObject lottoAfterUI;

    public GameObject lottoBeforeUI;
    private UIPanel lottoUiPanel;

    public GameObject mainScenePanel;

    private UIPanel mainSceneUiPanel;

    private GameObject pcMesh;
    private GameObject pcRig;
    private GameObject sign_overlap;
    public GameObject skinTitle;
    public GameObject backgroundSpriteObject;
    private UISprite backgroundSprite;

    private float popupDeltaTime;


    private AudioSource audioSource;
    public AudioClip NomalFlashClip;
    public AudioClip SpecialFlashClip;
    public AudioClip NomalResultClip;
    public AudioClip SpecialResultClip;

    private ParticleSystem backgroundParticle;

    private bool isflashEnd;

    // Use this for initialization
    private void Start() {
        mainSceneUiPanel = mainScenePanel.GetComponent<UIPanel>();
        lottoUiPanel = GetComponent<UIPanel>();
        pcMesh = GameObject.Find("pc_preview");
        pcRig = GameObject.Find("PC_rig");
        flashSprite = flashObject.GetComponent<TweenAlpha>();
        touchSprite = flashTouchGameObject.GetComponent<UISprite>();
        audioSource = GetComponent<AudioSource>();
        backgroundParticle = GameObject.Find("clear_background_particle").GetComponent<ParticleSystem>();
        sign_overlap = GameObject.Find("sign_overlap");
        backgroundSprite = backgroundSpriteObject.GetComponent<UISprite>();
        sign_overlap.SetActive(false);

        lottoAfterUI.SetActive(false);

        isLottoUsed = false;

        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        bgmMgr = dataMgr.GetComponent<BGMMgr>();
        popupDeltaTime = -1;
        
    }

    // Update is called once per frame
    private void Update() {
        lottoUiPanel.alpha = 1 - mainSceneUiPanel.alpha;

        audioSource.mute = dataMgr.dataClass.CURRENT_MUTE;
        bgmMgr.SetVolume(1 - flashSprite.alpha);
        if(isSpecial)
            touchSprite.alpha = flashSprite.alpha;
        else if(touchSprite.alpha > 0) {
            touchSprite.alpha = 0;
        }

        if (!isLottoActived) {
            if (!mainScenePanel.activeInHierarchy) mainScenePanel.SetActive(true);

            if (mainSceneUiPanel.alpha < 0.99f) mainSceneUiPanel.alpha = Mathf.Lerp(mainSceneUiPanel.alpha, 1, 0.3f);
            else if (mainSceneUiPanel.alpha != 1) mainSceneUiPanel.alpha = 1;


            return;
        }

        if (mainSceneUiPanel.alpha > 0.001f) {
            mainSceneUiPanel.alpha = Mathf.Lerp(mainSceneUiPanel.alpha, 0, 0.3f);
        }
        else if (mainSceneUiPanel.alpha != 0) {
            mainSceneUiPanel.alpha = 0;
            mainScenePanel.SetActive(false);
        }

        if (popupDeltaTime >= 0) {
            popupDeltaTime += Time.deltaTime;

            if (popupDeltaTime > 1) {
                popupDeltaTime = -1;
                var tmp = Instantiate(GetTicketPopUp, GameObject.Find("Anchor").transform);
                tmp.transform.localPosition = new Vector3(0, 0, -400);
            }
        }

        if (lottoAfterUI.activeInHierarchy) {
            var particle = backgroundParticle.main;

            if (isSpecial) {
                particle.startColor = Random.ColorHSV(0, 1, 0.5f, 0.5f, 1, 1);
                particle.startSpeed = 7;
                backgroundSprite.color = new Color(1, 0.8f, 0.2f);
            }
            else {
                particle.startColor = Color.white;
                particle.startSpeed = 5;
                backgroundSprite.color = Color.white;
            }

        }

        if (isLottoUsed) {
            flashDeltaTime += Time.deltaTime;

            if ((flashDeltaTime > 0.4f) && Input.GetMouseButtonUp(0)
                || flashDeltaTime > 0.6f && !isflashEnd && !isSpecial) {

                isflashEnd = true;

                audioSource.clip = isSpecial ? SpecialResultClip : NomalResultClip;
                audioSource.Play(0);

                isLottoUsed = false;
                lottoBeforeUI.SetActive(false);
                lottoAfterUI.SetActive(true);

                flashSprite.enabled = true;
                flashSprite.from = 1;
                flashSprite.to = 0;
                flashSprite.Reset();

                var pcScale = pcRig.GetComponent<TweenScale>();

                pcScale.enabled = true;
                pcScale.Reset();

                flashDeltaTime = 0;

                if (isOverlap) {
                    isOverlap = false;
                    sign_overlap.SetActive(true);
                    sign_overlap.GetComponent<TweenAlpha>().enabled = true;
                    sign_overlap.GetComponent<TweenAlpha>().Reset();

                    var gauge = dataMgr.dataClass.CURRENT_LOTTO_HALF_TICKET + 0.34f;
                    if (gauge > 1) gauge = 1;

                    if (gauge >= 1) {
                        gauge = 0;
                        dataMgr.dataClass.CURRENT_LOTTO_TICKET++;
                        popupDeltaTime = 0;
                    }
                    dataMgr.dataClass.CURRENT_LOTTO_HALF_TICKET = gauge;
                    dataMgr.SaveData();
                }

                flashObject.GetComponent<BoxCollider>().enabled = false;

            }
        }
    }

    public void SetLottoActive(bool isActive) {
        isLottoActived = isActive;

        if (isActive) {
            var tmp_scale = skinTitle.GetComponent<TweenScale>();

            tmp_scale.enabled = true;
            tmp_scale.Reset();

            lottoBeforeUI.SetActive(true);
            lottoAfterUI.SetActive(false);

            isLottoUsed = false;
        }
        else {
            pcRig.transform.localScale = Vector3.zero;

            lottoBeforeUI.SetActive(false);
            lottoAfterUI.SetActive(false);
        }

        sign_overlap.SetActive(false);
    }

    public void UseLotto(Mesh mesh, Texture2D texture) {
        isLottoUsed = true;

        flashSprite.enabled = true;
        flashSprite.from = 0;
        flashSprite.to = 1;
        flashSprite.Reset();

        flashDeltaTime = 0;

        var pcScale = pcRig.GetComponent<TweenScale>();

        pcScale.enabled = false;
        pcRig.transform.localScale = Vector3.zero;

        pcMesh.GetComponent<Renderer>().material.mainTexture = texture;
        pcMesh.GetComponent<SkinnedMeshRenderer>().sharedMesh = mesh;

        flashObject.GetComponent<BoxCollider>().enabled = true;

        audioSource.clip = isSpecial ? SpecialFlashClip : NomalFlashClip;
        audioSource.Play(0);

        isflashEnd = false;

        sign_overlap.SetActive(false);
    }
}