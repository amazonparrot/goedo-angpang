﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyTotalScoreLabel : MonoBehaviour {

    private DataMgr dataMgr;
    private UILabel label;

    private int totalScore;
    private int currentScore;

	// Use this for initialization
	void Start () {
	    label = GetComponent<UILabel>();
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();

        SetScore();

	}

    // Update is called once per frame
    void Update () {

        if (Input.GetMouseButtonUp(0)) {
            SetScore();
        }

    }

    private void SetScore() {
        int size = dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SIZE;
        totalScore = size * 3;

        currentScore = 0;

        for (int i = 0; i < size; i++) {
            currentScore += dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SCORE[i];
        }

        label.text = currentScore + " l " + totalScore;
    }
}
