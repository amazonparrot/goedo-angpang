﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LobyStageStartBt : MonoBehaviour {

    private DataMgr dataMgr;

    // Use this for initialization
    void Start () {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();

    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnClick() {

#if UNITY_ANDROID
        if (
            !dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_UNLOCK[
                dataMgr.dataClass.CURRENT_STAGE_ID]) {
            return;
        }

#endif

        if (dataMgr.dataClass.CURRENT_STAGE_ID == 0) {
            StartCoroutine(DataMgr.SetScene("CartoonScene"));
        }
        else {
            //SceneManager.LoadScene("CutCartoonScene");
            StartCoroutine(DataMgr.SetScene("InGameScene"));
        }

    }
}
