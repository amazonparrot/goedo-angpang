﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyMoveEpisodeBt : MonoBehaviour {

    public bool isMoveNext;

    private LobyCameraMgr lobyCameraMgr;

	// Use this for initialization
	void Start () {
	    lobyCameraMgr = GameObject.Find("CameraManager").GetComponent<LobyCameraMgr>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnClick() {
        lobyCameraMgr.OnMoveEpisode(isMoveNext);
    }
}
