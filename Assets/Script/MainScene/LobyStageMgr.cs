﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyStageMgr : MonoBehaviour {

    public GameObject DataManager = null;

    public GameObject EpisodeTitleId = null;
    public GameObject EpisodeTitleName = null;
    public GameObject MovableObject = null;
    public GameObject GameExitUIObject;

    public GameObject StageInfo = null;

    public int CurrentStageId;
    public string[] StageTitle;

    private LobyCameraMgr cameraMgr;
    private LobyStageScoreUI stageScoreUi;
    private DataMgr dataMgr;
    private Vector3 UIPosition;

    private Vector3 EpisodeTitleIdPos;
    private Vector3 EpisodeTitleNamePos;
    private Vector3 MovableObjPos;
    private Vector3 StageInfoPos;

    private bool isMouseUp;

    void Awake() {
        GameObject obj = GameObject.FindWithTag("Data");

        if (obj == null) {
            obj = Instantiate(DataManager);
            obj.transform.name = "DataManager";
        }
    }

    // Use this for initialization
    void Start () {
	    cameraMgr = GameObject.Find("CameraManager").GetComponent<LobyCameraMgr>();
        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        stageScoreUi = GameObject.Find("stage_scores").GetComponent<LobyStageScoreUI>();

        EpisodeTitleIdPos = EpisodeTitleId.transform.localPosition;
        EpisodeTitleNamePos = EpisodeTitleName.transform.localPosition;
        MovableObjPos = MovableObject.transform.localPosition;
        StageInfoPos = StageInfo.transform.localPosition;
        StageTitle = new string[cameraMgr.MaximumStageId + 1];

        {
            StageTitle[0] = "블러썸 백작과 에메랄드 반지";
            StageTitle[1] = "유니랜드와 아이들";
            StageTitle[2] = "찰스와 케이크 공장";

        }

        isMouseUp = false;
        CurrentStageId = dataMgr.dataClass.CURRENT_EPISODE_ID + 1;

        EpisodeTitleId.GetComponent<UILabel>().text = "E P I S O D E  " + CurrentStageId;
        string script = dataMgr.GetComponent<ScriptLanguageMgr>().Find(StageTitle[CurrentStageId - 1], 2 + CurrentStageId);
        EpisodeTitleName.GetComponent<UILabel>().text = script;
    }

    // Update is called once per frame
    void Update () {

        if (Application.platform == RuntimePlatform.Android) {

            if (Input.GetKeyUp(KeyCode.Escape)) {

                if (GameObject.Find("pop_lotto_panel").GetComponent<LottoPanel>().isLottoActived) {
                    var lotto = GameObject.Find("pop_lotto_panel").GetComponent<LottoPanel>();
                    if (!lotto.isLottoUsed)
                        lotto.isLottoActived = false;
                }
                else {
                    Instantiate(GameExitUIObject, GameObject.Find("Anchor").transform);
                }

            }

        }

        if (!cameraMgr.isRelax) {

            if (Input.GetMouseButton(0)) {
                float position = -cameraMgr.GetTempPosition();
                UIPosition.x = position * (Mathf.Abs(position) > 0.5f ? 200 : 1);
            }
            else if (Input.GetMouseButtonUp(0)) {
                isMouseUp = true;
                CurrentStageId = cameraMgr.stageId + 1;
                dataMgr.dataClass.CURRENT_EPISODE_ID = cameraMgr.stageId;
                dataMgr.SaveData();
                if (!cameraMgr.isSameStage) {
                    UIPosition.x = cameraMgr.isMoveNext ? 500 : -500;
                    EpisodeTitleId.GetComponent<UILabel>().text = "E P I S O D E " + CurrentStageId;
                    string script = dataMgr.GetComponent<ScriptLanguageMgr>().Find(StageTitle[CurrentStageId - 1], 2 + CurrentStageId);
                    EpisodeTitleName.GetComponent<UILabel>().text = script;
                    stageScoreUi.GetScore();
                }

            }


            SetPosition();

	    }

        if (isMouseUp) {
            UIPosition.x = Mathf.Lerp(UIPosition.x, 0, Time.deltaTime * 15);
            SetPosition();
            if (Mathf.Abs(UIPosition.x) < 0.001f) {
                isMouseUp = false;
                UIPosition.x = 0;
            }
        }
	}

    private void SetPosition() {
        EpisodeTitleId.transform.localPosition = EpisodeTitleIdPos + UIPosition;
        EpisodeTitleName.transform.localPosition = EpisodeTitleNamePos + UIPosition;
        MovableObject.transform.localPosition = MovableObjPos + UIPosition;
        StageInfo.transform.localPosition = StageInfoPos + UIPosition;
    }
}
