﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESwitchTile : SwitchTile {

    private bool isSaved = false;
    private StageMgr stageMgr;

    private new void Start() {
        base.Start();
        stageMgr = GameObject.Find("StageManager").GetComponent<StageMgr>();
    }


    public override void SetLocalX(float x) {
        base.SetLocalX(x);

        if (isUsed && !isSaved) {
            isSaved = true;

            GameObject.Find("TileManager").GetComponent<ETileMgr>().AddTicketList(id, stageMgr.GetCurrentStageId());

        }
    }

    public override void SetLocalZ(float z) {
        base.SetLocalZ(z);

        if (isUsed && !isSaved) {
            isSaved = true;

            GameObject.Find("TileManager").GetComponent<ETileMgr>().AddTicketList(id, stageMgr.GetCurrentStageId());

        }
    }
}
