﻿using UnityEngine;

public class ETotalRingUI : TotalRingUI {

    private ETileMgr tileMgr;

    public GameObject ticketSpriteObject;

    private TweenAlpha ticketAlpha;
    private bool isInit;

    private new void Start() {
        base.Start();

        tileMgr = GameObject.Find("TileManager").GetComponent<ETileMgr>();
        ticketAlpha = ticketSpriteObject.GetComponent<TweenAlpha>();
    }

    // Update is called once per frame
    private new void Update() {
        base.Update();

        if (gameSystemMgr.m_switchMaximum > 0) {
            label.text = gameSystemMgr.m_switchCount + "/" + gameSystemMgr.m_switchMaximum;

            if (!isInit) {
                isInit = true;

                ticketAlpha.enabled = true;
            }
        }
    }


}