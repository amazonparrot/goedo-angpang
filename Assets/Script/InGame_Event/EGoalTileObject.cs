﻿public class EGoalTileObject : GoalTileObject {

    public override void SetLocalX(float x) {

        if ((player.positionId.y == id.y) && (player.positionId.z == id.z)) {

            if (stageNextId == -2) return;

            gameSystemMgr.isPortalArrived = true;
            gameSystemMgr.isCleared = true;
            gameSystemMgr.m_nextStageId = stageNextId;
        }
    }

    public override void SetLocalZ(float z) {

        if ((player.positionId.y == id.y) && (player.positionId.x == id.x)) {

            if (stageNextId == -2) return;

            gameSystemMgr.isPortalArrived = true;
            gameSystemMgr.isCleared = true;
            gameSystemMgr.m_nextStageId = stageNextId;
        }
    }
}