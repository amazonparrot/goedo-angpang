﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EGameSystemMgr : GameSystemMgr {

    private bool isEnd = false;


    private new void Start() {
        base.Start();

        isAnalyticsEnabled = false;
    }
	
	// Update is called once per frame
    private new void Update () {
		base.Update();

        if (isCleared) {
            if (!isEnd) {
                isEnd = true;

                GameObject.Find("TileManager").GetComponent<ETileMgr>().SaveHalloween();
                GameObject.Find("DataManager").GetComponent<DataMgr>().dataClass.CURRENT_LOTTO_TICKET += m_switchCount;

            }
        }


	}
}
