﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using Random = System.Random;

[System.Serializable]
public class TicketID {

    public int x;
    public int y;
    public int z;

    public int stage;

    public Vector3 GetVector3() {
        return new Vector3(x, y, z);
    }

    public void SetVector3(Vector3 vec3) {
        x = (int) vec3.x;
        y = (int) vec3.y;
        z = (int) vec3.z;
    }
}

[System.Serializable]
public class HalloweenData {

    [SerializeField] public List<TicketID> ticketList;

}

public class ETileMgr : TileMgr {

    public HalloweenData halloweenData;

    void Start() {
        isResetTilePosition = true;
        isClicked = false;
        isMouseUp = false;
        CameraManager = GameObject.Find("CameraManager");
        _cameraMgr = CameraManager.GetComponent<CameraMgr>();
        gameSystemMgr = GameSystemManager.GetComponent<GameSystemMgr>();
        stageMgr = StageManager.GetComponent<StageMgr>();
        MainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        _mSelectedTileId = new Vector3(-1, -1, -1);
        m_tileShortcut = new ArrayList();
        isLoad = false;
        rand = new Random();
        _mNonTilemap = new List<GameObject>();

        isAnalyticsEnabled = false;

        Load();

    }

    protected new void Load() {
        DataMgr dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();

        int stageId = 1;
        if (dataMgr != null) dataMgr.SetLoading(true);


        string path;


        if (Application.platform == RuntimePlatform.Android) {
            path = "jar:file://" + Application.dataPath + "!/assets" + "/Event" + "/" + stageId + ".map";
        }
        else {
            path = "file://" + Application.streamingAssetsPath + "/Event" + "/" + stageId + ".map";


        }

        Debug.Log("Event Load Ready : " + path);

        if (!LoadHalloween()) {
            halloweenData = new HalloweenData();

            halloweenData.ticketList = new List<TicketID>();
        }

        Debug.Log("Skiped Ticket Count : " + halloweenData.ticketList.Count);

        for (int i = 0; i < halloweenData.ticketList.Count; i++) {
            Debug.Log(halloweenData.ticketList[i].GetVector3());
        }

        StartCoroutine(Parse(path));


    }

    private IEnumerator Parse(string mapPath) {
        if (mapPath.Length == 0) yield return null;

        WWW File = new WWW(mapPath);
        yield return File;

        byte[] bConvert = Encoding.Convert(Encoding.UTF8, Encoding.Default, File.bytes);

        // utf-8 인코딩

        //byte[] bytesForEncoding = Encoding.UTF8.GetBytes(File.text);
        //string encodedString = Convert.ToBase64String(bytesForEncoding);
        string encodedString = System.Text.Encoding.Default.GetString(bConvert);
        string source;
        string[] sources = encodedString.Split('\n');


        if (sources.Length == 0) {
            yield return File;
        }


        string[] values = sources[0].Split(',');

        int stageLength = 0;
        ArrayList stringBuilder = null;
        bool isStageAvailable = false;

        int index = 0;

        while (index < sources.Length) {

            source = sources[index];
            source = source.Replace(" ", "");
            values = source.Split(',');

            //stage 설정
            if (values[0] == "#STAGE") {
                stageLength = int.Parse(values[1]);
                index++;
                continue;

            }

            //포탈타일
            if (values[0] == "PortalTile") {
                TileShortcut tmp_trace = FindTargetTileShortcut(values[5]);
                Color tmp_color;
                if (tmp_trace.name == null) {
                    tmp_color = UnityEngine.Random.ColorHSV(0, 1, 0.925f, 0.925f, 1, 1);
                }
                else {
                    tmp_color = tmp_trace.portalColor;
                }
                TileShortcut tmp =
                    new TileShortcut(new Vector3(int.Parse(values[1]), int.Parse(values[2]), int.Parse(values[3])),
                        stageLength, values[5], values[6], tmp_color);
                m_tileShortcut.Add(tmp);
            }

            index++;

        }

        index = 0;
        stageLength = 0;
        while (index < sources.Length) {
            source = sources[index];
            source = source.Replace(" ", "");
            values = source.Split(',');

            if (source == "#end") {
                break;
            }

            if (values.Length == 0) {
                break;
            }

            if (values[0] == "#Speech") {
                string str = values[4].Replace("_", " ");
                prev_tile.SetSpeechBubble(str);
                index++;
                continue;
            }

            //player count
            if (values[0] == "#MOVEMENT") {
                gameSystemMgr.m_playerMovementMaximum[0] = int.Parse(values[1]);
                gameSystemMgr.m_playerMovementMaximum[1] = int.Parse(values[2]);
                index++;
                continue;
            }

            //map size
            if (values.Length == 3) {
                if (stringBuilder == null) {
                    stringBuilder = new ArrayList();
                }
                stringBuilder.Insert(0, source);
                index++;
                continue;
            }

            //stage 설정
            if (values[0] == "#STAGE") {
                //첫 스테이지일 경우
                if (!isStageAvailable) {
                    stringBuilder = new ArrayList();
                    isStageAvailable = true;
                    index++;
                    stageLength = int.Parse(values[1]);
                    continue;
                }
                int tmp = int.Parse(values[1]);
                stageMgr.AddStageString(stringBuilder, stageLength);
                stageLength = tmp;
                stringBuilder = new ArrayList();
                index++;
                continue;

            }

            if (values[0] == "RingTile") {
                Vector3 tilePos = new Vector3(int.Parse(values[1]), int.Parse(values[2]), int.Parse(values[3]));

                bool isUsed = false;
                for (int i = 0; i < halloweenData.ticketList.Count; i++) {
                    if (halloweenData.ticketList[i].GetVector3() == tilePos) {
                        if(halloweenData.ticketList[i].stage == stageLength)
                        isUsed = true;
                        break;
                    }
                }

                if (isUsed) {
                    index++;
                    continue;
                }

            }

            if (values.Length == 4 || values.Length == 5 || values[0] == "NpcDogTile") {
                if (stringBuilder == null) {
                    stringBuilder = new ArrayList();
                }
                stringBuilder.Add(ParseTile(values));
            }

            //포탈타일
            if (values[0] == "PortalTile") {

                if (stringBuilder == null) {
                    stringBuilder = new ArrayList();
                }
                stringBuilder.Add(ParseTile(values));
            }

            index++;
        }

        if (stringBuilder != null) {
            stageMgr.AddStageString(stringBuilder, stageLength);
        }

        SettingMap(0);
        isLoad = true;
        GameObject.FindWithTag("Data").GetComponent<DataMgr>().SetLoading(false);

        Debug.Log("Load End");

    }

    private bool LoadHalloween() {
        bool _fhile_check = File.Exists(DataMgr.pathForDocumentsFile(Application.persistentDataPath + "/halloween2017.dat"));

        if (!_fhile_check) {
            return false;
        }
        BinaryFormatter _binary_formatter = new BinaryFormatter();


        FileStream _filestream = File.Open((Application.persistentDataPath + "/halloween2017.dat"), FileMode.Open);


        halloweenData = (HalloweenData)_binary_formatter.Deserialize(_filestream);

        _filestream.Close();

        return true;
    }

    public void SaveHalloween() {
        BinaryFormatter _binary_formatter = new BinaryFormatter();
        FileStream _filestream = File.Create(Application.persistentDataPath + "/halloween2017.dat");

        _binary_formatter.Serialize(_filestream, halloweenData);

        _filestream.Close();
    }

    public void AddTicketList(Vector3 posId, int stageId) {
        var id = new TicketID();
        id.stage = stageId;
        id.SetVector3(posId);
        halloweenData.ticketList.Add(id);
    }
}
