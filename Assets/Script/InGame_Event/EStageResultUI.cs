﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EStageResultUI : StageResultUI {


    public GameObject TotalTicketGameObject;

    private EClearTicketLabel clearTicketLabel;


    private new void Start() {
        base.Start();

        clearTicketLabel = TotalTicketGameObject.GetComponent<EClearTicketLabel>();
    }

    // Update is called once per frame
    void Update() {
        if (gameSystemMgr.isCleared || gameSystemMgr.isFailed) {

            if (accrueTime == 0 && gameSystemMgr.isFailed) {
                //stage_result.GetComponent<UISprite>().spriteName = "result_txt_over";
                transform.localPosition = Vector3.zero;
                if (!isPlayed) {

                    //광고
                    if (!dataMgr.dataClass2.CURRENT_ATZ) {
                        int r = Random.Range(0, 3);

                        if (r == 0) {
                            ShowRewardedVideo();
                        }
                    }

                    audioSource.mute = dataMgr.dataClass.CURRENT_MUTE;
                    audioSource.clip = FailAudioClip;
                    audioSource.Play();
                    isPlayed = true;
                }

            }
            else if (accrueTime >= 1 && gameSystemMgr.isCleared &&
                !clear_background.GetComponent<TweenScale>().enabled) {
                transform.localPosition = Vector3.zero;
                clear_background.GetComponent<TweenScale>().enabled = true;
                if (!isPlayed) {
                    audioSource.mute = dataMgr.dataClass.CURRENT_MUTE;
                    audioSource.clip = ClearAudioClip;
                    audioSource.Play();
                    isPlayed = true;
                }
            }

            if (gameSystemMgr.isCleared) {
                if (clear_layer2 == null && accrueTime >= 1.5f) {
                    clear_layer2 = GameObject.Find("clear_panel_layer_2");
                    clear_layer2.GetComponent<TweenScale>().enabled = true;
                    clearTicketLabel.SetEnableLabel();
                    particle.SetActive(true);
                }

                if (clear_layer3 == null && accrueTime >= 2.5f) {
                    clear_layer3 = GameObject.Find("clear_panel_layer_3");
                    clear_layer3.GetComponent<TweenScale>().enabled = true;
                }
            }

            accrueTime += Time.deltaTime;
        }
    }
}
