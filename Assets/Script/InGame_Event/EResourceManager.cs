﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EResourceManager : ResourceManager {

    // Use this for initialization
    private void Start() {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        Load();
    }


    public new void Load() {
        var stageId = 1;


        //BackgroundObject.GetComponent<RawImage>().texture = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Textures/Bg/stage" + stageID + "_background.png");
        BackgroundObject.GetComponent<RawImage>().texture =
            Resources.Load<Texture2D>("Bg/event" + "/stage" + stageId + "_background");

        var tileRenderer = new Renderer[8];

        tileRenderer[0] = GrassTile.GetComponent<Renderer>();
        tileRenderer[1] = NomalTile.GetComponent<Renderer>();
        tileRenderer[2] = AwesomeTile.GetComponent<Renderer>();
        tileRenderer[3] = FireTile.GetComponent<Renderer>();
        tileRenderer[4] = SpawnTile.GetComponent<Renderer>();
        tileRenderer[5] = GoalTile.GetComponent<Renderer>();
        tileRenderer[6] = PortalTile.GetComponent<Renderer>();
        tileRenderer[7] = VanishTile.GetComponent<Renderer>();

        Texture2D[] textures;
        textures = Resources.LoadAll<Texture2D>("Tile/event" + "/Stage_0" + stageId);
        Debug.Log("textures count : " + textures.Length);

        for (var i = 0; i < textures.Length; i++) {
            //Texture2D tmp = AssetDatabase.LoadAssetAtPath<Texture2D>(objs[i]);
            var tmp = textures[i];

            switch (tmp.name.Split('_')[0]) {
                case "GrassTile":
                    tileRenderer[0].material.mainTexture = tmp;
                    break;
                case "NomalTile":
                case "TileBase":
                    tileRenderer[1].material.mainTexture = tmp;
                    break;
                case "AwesomeTile":
                    tileRenderer[2].material.mainTexture = tmp;
                    break;
                case "FireTile":
                    tileRenderer[3].material.mainTexture = tmp;
                    break;
                case "SpawnTile":
                    tileRenderer[4].material.mainTexture = tmp;
                    break;
                case "GoalTile":
                    tileRenderer[5].material.mainTexture = tmp;
                    break;
                case "PortalTile":
                    tileRenderer[6].material.mainTexture = tmp;
                    break;
                case "VanishTile":
                    tileRenderer[7].material.mainTexture = tmp;
                    break;
            }
        }
    }
}
