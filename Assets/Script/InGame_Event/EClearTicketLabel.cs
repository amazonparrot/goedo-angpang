﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EClearTicketLabel : MonoBehaviour {

    private UILabel label;
    private GameSystemMgr gameSystemMgr;

    private TweenPosition tweenPosition;
    private TweenAlpha tweenAlpha;

	// Use this for initialization
	void Start () {
	    gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
	    label = GetComponent<UILabel>();

	    tweenPosition = GetComponent<TweenPosition>();
	    tweenAlpha = GetComponent<TweenAlpha>();

	    tweenPosition.enabled = false;
	    tweenAlpha.enabled = false;

    }
	
	// Update is called once per frame
	void Update () {
	    label.text = gameSystemMgr.m_switchCount + "";
	}

    public void SetEnableLabel() {
        tweenPosition.enabled = true;
        tweenAlpha.enabled = true;

        tweenPosition.Reset();
        tweenAlpha.Reset();
    }
}
