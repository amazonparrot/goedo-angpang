﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Purchasing;
using Random = UnityEngine.Random;

public class BuyTicketIAP : MonoBehaviour, IStoreListener {

    public bool isProcessed;

    private string IAPResultCode = "";

    private IStoreController m_StoreController = null;
    private IExtensionProvider m_StoreExtensionProvider = null;

    private string BuyTicketsPurchaseId = "product_buyticket_id_01";

    private DataMgr dataMgr;

    public void OnInitializeFailed(InitializationFailureReason error) {
        IAPResultCode = "INIT_FAILED";
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {

        isProcessed = true;

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
            // 구매한 상품 ID 를 비교해서 그 상품에 맞는 처리를 해줍니다.
            if (string.Equals(args.purchasedProduct.definition.id, BuyTicketsPurchaseId)) {
                // 저는 결제 서버를 운용하지 않으므로 결제 결과에 대한 검증처리는 하지 않아 어뷰징에 취약할 수 있습니다.
                // 여유가 있으시면 결제 서버를 구축하셔서 이 부분에서 검증하신 후 처리하시면 됩니다. 

                int randomCode = Random.Range(0, 1000000);

                IAPResultCode = DateTime.Now.ToString("O") + "BT" + randomCode;

                dataMgr.dataClass.CURRENT_LOTTO_SPECIAL_TICKET++;
                dataMgr.dataClass.CURRENT_LOTTO_TICKET += 30;
                dataMgr.SaveData();

                if (dataMgr.dataClass3.TICKETS_HISTORY == null)
                    dataMgr.dataClass3.TICKETS_HISTORY = new List<string>();

                dataMgr.dataClass3.TICKETS_HISTORY.Add(IAPResultCode);
                dataMgr.SaveData3();


                Analytics.CustomEvent("Purchase", new Dictionary<string, object> {
                    {"Item", args.purchasedProduct.definition.id},
                    {"Code", IAPResultCode},
                    {"Result", "IDK"}
                });

                return PurchaseProcessingResult.Complete;
            }
        }
        else {
            int randomCode = Random.Range(0, 1000000);

            IAPResultCode = DateTime.Now.ToString("O") + "NONMOBILE" + randomCode;

            Analytics.CustomEvent("Purchase", new Dictionary<string, object> {
                {"Item", args.purchasedProduct.definition.id},
                {"Code", IAPResultCode},
                {"Result", "IDK"}
            });

            dataMgr.dataClass.CURRENT_LOTTO_SPECIAL_TICKET++;
            dataMgr.dataClass.CURRENT_LOTTO_TICKET += 30;
            dataMgr.SaveData();

            if (dataMgr.dataClass3.TICKETS_HISTORY == null)
                dataMgr.dataClass3.TICKETS_HISTORY = new List<string>();

            dataMgr.dataClass3.TICKETS_HISTORY.Add(IAPResultCode);
            dataMgr.SaveData3();

        }

        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product i, PurchaseFailureReason p) {
        IAPResultCode = "PURCHASE_FAILED";
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }

    public void purchase(string id) {
        if (isInitialized()) {
            Product product = m_StoreController.products.WithID(id);
            if (product != null && product.availableToPurchase)
                m_StoreController.InitiatePurchase(product);
        }
    }

    public string GetIAPCode() {
        return IAPResultCode;
    }

    // Use this for initialization
    private void Start() {

        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();


        if (null == m_StoreController) {
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            // IAP 초기화할 때 상품 ID 목록을 넣어줍니다.
            // 디벨로퍼 콘솔에서 상품 등록한 그 ID 입니다.
            //builder.AddProduct(BuyTicketsPurchaseId, ProductType.Consumable);
            builder.AddProduct(BuyTicketsPurchaseId, ProductType.Consumable, new IDs {
                {BuyTicketsPurchaseId, AppleAppStore.Name},
                {BuyTicketsPurchaseId, GooglePlay.Name},
            });

            UnityPurchasing.Initialize(this, builder);
        }
    }

    // Update is called once per frame
    private void Update() {
    }

    bool isInitialized() {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }
}
