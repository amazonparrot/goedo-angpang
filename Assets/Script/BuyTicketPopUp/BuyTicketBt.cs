﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyTicketBt : MonoBehaviour {

    public GameObject CodeLabel;
	public GameObject Panel;
    private UILabel label;
	private UIPanel _uiPanel;

    private BuyTicketIAP buyTicketsIAP;
    private string BuyTicketsPurchaseId = "product_buyticket_id_01";


    // Use this for initialization
    void Start() {
        buyTicketsIAP = GetComponent<BuyTicketIAP>();
        label = CodeLabel.GetComponent<UILabel>();
		_uiPanel = Panel.GetComponent<UIPanel> ();
    }

    // Update is called once per frame
    void Update() {
        if (buyTicketsIAP.isProcessed) {
            label.text = "결제 코드 : " + buyTicketsIAP.GetIAPCode();

			if (_uiPanel.alpha > 0.01f)
				_uiPanel.alpha = Mathf.Lerp(_uiPanel.alpha, 0, 0.1f);
			else {
				_uiPanel.alpha = 0;
				Destroy(Panel);
				return;
			}

        }
        else if (buyTicketsIAP.GetIAPCode() != "") {
            label.text = "에러 : " + buyTicketsIAP.GetIAPCode();
        }
    }

    void OnClick() {
        if (!buyTicketsIAP.isProcessed)
            buyTicketsIAP.purchase(BuyTicketsPurchaseId);
    }
}
