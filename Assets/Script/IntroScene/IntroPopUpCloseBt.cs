﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPopUpCloseBt : MonoBehaviour {


    public GameObject PopUpPanel;
    public GameObject PopUpGameObject;
    private TweenScale popUpScale;
    private IntroPopUpUI introPopUpUI;

    // Use this for initialization
    void Start () {
	    popUpScale = PopUpGameObject.GetComponent<TweenScale>();
        introPopUpUI = PopUpPanel.GetComponent<IntroPopUpUI>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnClick() {
        popUpScale.enabled = true;
        introPopUpUI.SetClosed();
    }
}
