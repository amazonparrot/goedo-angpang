﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPopUpUI : MonoBehaviour {

    public GameObject[] popups;
    public bool isAllClosed;
    private bool isLastPopUpClosed;
    private int currentPopUps;
    private int index;

	// Use this for initialization
	void Start () {
	    currentPopUps = popups.Length;
	    index = 0;

	    for (int i = 0; i < popups.Length; i++) {
	        popups[i].SetActive(false);
	    }
        popups[index].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0) && currentPopUps <= 0) {
	        isAllClosed = true;
	    }
	}

    public void SetClosed() {
        currentPopUps--;
        index++;

        if (currentPopUps <= 0) {
            isLastPopUpClosed = true;
        }
        else {
            popups[index].SetActive(true);
        }
    }
}
