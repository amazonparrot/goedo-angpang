﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroLogoMgr : MonoBehaviour {

    public GameObject DataManager = null;

    public GameObject AmazonParrot;
    public GameObject LogoPanel;
    public GameObject IntroPanel;
    public GameObject PopUpPanel;

    private IntroPopUpUI introPopUpUi;

    private DataMgr dataMgr;

    private TweenAlpha IntroPanelAlpha;

    private float Delay_AmazonParrot;

    private bool isLogoEnded;
    private bool isIntroStarted;
    private bool isIntroPopUpActive;

    private float AccrueTime;

    void Awake() {
        GameObject obj = GameObject.FindWithTag("Data");

        if (obj == null) {
            obj = Instantiate(DataManager);
            obj.transform.name = "DataManager";
        }

        DataMgr.SetUIRootScaleStyle();

    }

    // Use this for initialization
    void Start () {
        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();

        AmazonParrot.SetActive(false);
        IntroPanelAlpha = LogoPanel.GetComponent<TweenAlpha>();
        LogoPanel.GetComponent<UIPanel>().alpha = 1;
        IntroPanel.SetActive(false);
        introPopUpUi = PopUpPanel.GetComponent<IntroPopUpUI>();
        PopUpPanel.SetActive(false);

        Delay_AmazonParrot = 3.5f;
        isLogoEnded = false;
        isIntroStarted = false;
        AccrueTime = -0.1f;

        isIntroPopUpActive = !dataMgr.dataClass.CURRENT_NOT_TUTORIAL;
        dataMgr.dataClass.CURRENT_NOT_TUTORIAL = true;
        dataMgr.SaveData();


    }

    // Update is called once per frame
    void Update () {

	    AccrueTime += Time.deltaTime;

	    if (!AmazonParrot.activeSelf && AccrueTime >= 0) {
            GameObject.Find("loading").GetComponent<TweenScale>().enabled = true;
            AmazonParrot.SetActive(true);
	    }

        if (AccrueTime > Delay_AmazonParrot && !isLogoEnded) {
            isLogoEnded = true;
            IntroPanelAlpha.enabled = true;
            IntroPanel.SetActive(true);
            GameObject.Find("DataManager").GetComponent<BGMMgr>().ForcePlay();
        }

        if (Input.GetMouseButtonUp(0)) {
	        if (AccrueTime > Delay_AmazonParrot && !isLogoEnded) {
            }
            else if(isIntroStarted) {

	            if (isIntroPopUpActive) {
	                PopUpPanel.SetActive(true);

	                if (introPopUpUi.isAllClosed)
                        StartCoroutine(DataMgr.SetScene("MainScene"));
	            }
	            else {
                    StartCoroutine(DataMgr.SetScene("MainScene"));
                }
            }
        }

	    if (!isIntroStarted && isLogoEnded && !IntroPanelAlpha.enabled) {
	        isIntroStarted = true;
	    }


	}
}
