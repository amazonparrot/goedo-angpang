﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSelectTexture : MonoBehaviour {


	public UIAtlas AtlasJPN;
	public UIAtlas AtlasENG;
	private UIAtlas AtlasKOR;

	public string spriteName;

	private UISprite sprite = null;
	private ScriptLanguageMgr languageMgr = null;
	private LANG CurrentLanguage;


	// Use this for initialization
	void Start () {
		sprite = GetComponent<UISprite>();
		languageMgr = GameObject.Find("DataManager").GetComponent<ScriptLanguageMgr>();
		AtlasKOR = sprite.atlas;

		SetLanguage();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetLanguage() {
		CurrentLanguage = languageMgr.CurrentLanguage;
		spriteName = sprite.spriteName;

		switch (CurrentLanguage)
		{
			case LANG.JAPANESE:
				sprite.atlas = AtlasJPN;
				sprite.spriteName = spriteName;
				break;
			case LANG.ENGLISH:
				sprite.atlas = AtlasENG;
				sprite.spriteName = spriteName;
				break;
			default:
				sprite.atlas = AtlasKOR;
				sprite.spriteName = spriteName;
				break;
		}
	}
}
