﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRenderQueue : MonoBehaviour {

    public int renderque;

	// Use this for initialization
	void Start () {
        Renderer[] renderers = this.transform.GetComponentsInChildren<Renderer>(true);
		for (int i = 0; i < renderers.Length; ++i) {
			if (renderers[i].sharedMaterials == null) continue; 
			for (int j = 0; j < renderers[i].sharedMaterials.Length; ++j) { 
				if (renderers[i].sharedMaterials[j] != null) { 
					renderers[i].material.renderQueue = renderque;
				}
			} 
		}

    }

    // Update is called once per frame
    void Update () {

        

    }
}
