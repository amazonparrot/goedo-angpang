﻿using UnityEngine;

public class ClosetPlayerRotation : MonoBehaviour {
    private float mousePos;

    private float mouseStartPos;
    private float mouseDeltaPos;

    // Use this for initialization
    private void Start() {
        mousePos = 0;
    }

    // Update is called once per frame
    private void Update() {

        if (Input.GetMouseButtonDown(0)) {
            mouseDeltaPos = Input.mousePosition.x;
        }

        if (Input.GetMouseButton(0) && Mathf.Abs(Input.mousePosition.x - mouseDeltaPos) > 5) {
            transform.Rotate(Vector3.down*(Input.mousePosition.x - mouseStartPos));
            mouseStartPos = Input.mousePosition.x;
        }
    }
}