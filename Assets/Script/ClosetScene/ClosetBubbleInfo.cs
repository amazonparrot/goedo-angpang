﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetBubbleInfo : MonoBehaviour {

    private TweenScale scale;

    private bool isActived;
    private float deltaTime;


	// Use this for initialization
	void Start () {
	    scale = GetComponent<TweenScale>();
	    scale.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	    if (isActived) {
	        deltaTime += Time.deltaTime;

	        if (deltaTime > 5) {
	            isActived = false;

                scale.enabled = true;
                scale.@from = Vector3.one;
                scale.to = Vector3.zero;

                scale.Reset();

            }

	    }
	}

    public void SetBubbleActive() {

        scale.enabled = true;
        scale.@from = Vector3.zero;
        scale.to = Vector3.one;

        scale.Reset();

        isActived = true;
        deltaTime = 0;

    }

}
