﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetScrollbarUI : MonoBehaviour {

    public GameObject ScrollBar;
    public GameObject ScrollViewInfo;
    public GameObject SkinButton;
    private GameObject player;
    private UILabel slotLabel;

    private DataMgr dataMgr;
    private UIScrollBar _uiScrollBar;

    private ClosetList closetList;
    

    private float MaximumBarHeight;

	// Use this for initialization
	void Start () {

        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
	    _uiScrollBar = ScrollBar.GetComponent<UIScrollBar>();
	    slotLabel = GameObject.Find("Slot_text").GetComponent<UILabel>();
	    closetList = dataMgr.GetComponent<ClosetList>();
	    slotLabel.text = closetList.ClosetCategory[dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX];


        Texture2D[] textures;
	    textures = Resources.LoadAll<Texture2D>("pcskin/" + closetList.ClosetCategoryPath[dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX] + "/map");
        Debug.Log("length : " + textures.Length);

	    for (int i = 0; i < textures.Length; i++) {
            GameObject tmp = Instantiate(SkinButton, ScrollViewInfo.transform);
            Debug.Log("texture name : " + textures[i].name);
	        Mesh tmp_mesh = Resources.Load<Mesh>("pcskin/" + closetList.ClosetCategoryPath[dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX] + "/fbx/" + textures[i].name);

	        tmp.transform.name = "bt_skin_" + i;
            tmp.GetComponent<ClosetScrollInfoBt>().SetId(i);
	        tmp.GetComponent<ClosetScrollInfoBt>().texture = textures[i];
	        tmp.GetComponent<ClosetScrollInfoBt>().mesh = tmp_mesh;

            float x = -211.8f + 129.2f * (i % 4);
	        float y = -129.2f * (int)(i / 4);

            tmp.transform.localPosition = new Vector3(x, y, 0);
            tmp.GetComponent<ClosetScrollInfoBt>().Refresh();
	    }

	    if (textures.Length > 12) {
	        int length = textures.Length - 8;
            MaximumBarHeight = -129.2f * (int)(length / 4);
        }
	    else {
	        MaximumBarHeight = 0;
	    }
        Debug.Log("texture skin0" + (dataMgr.dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX + 1));

	    dataMgr.playerTexture =
	        Resources.Load<Texture2D>("pcskin/" + closetList.ClosetCategoryPath[dataMgr.dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX] + "/map/" +
	                                  dataMgr.dataClass.CURRENT_PLAYER_SKIN);
	    dataMgr.playerMesh =
	        Resources.Load<Mesh>("pcskin/" + closetList.ClosetCategoryPath[dataMgr.dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX] + "/fbx/" +
	                             dataMgr.playerTexture.name);

        player = GameObject.Find("pc_mesh");
        player.GetComponent<Renderer>().material.mainTexture = dataMgr.playerTexture;
	    player.GetComponent<SkinnedMeshRenderer>().sharedMesh = dataMgr.playerMesh;

	}
	
	// Update is called once per frame
	void Update () {

        if (Application.platform == RuntimePlatform.Android) {

            if (Input.GetKeyUp(KeyCode.Escape)) {

                StartCoroutine(DataMgr.SetScene("MainScene"));

            }

        }

        ScrollViewInfo.transform.localPosition = new Vector3(0, 131 - MaximumBarHeight * _uiScrollBar.scrollValue, 0);
		
	}

    public void Refresh() {

        slotLabel.text = closetList.ClosetCategory[dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX];


        for (var i = 0; i < ScrollViewInfo.transform.childCount; i++) {
            var tmp = ScrollViewInfo.transform.GetChild(i).gameObject;
            Destroy(tmp);
        }

        Texture2D[] textures;
        textures = Resources.LoadAll<Texture2D>("pcskin/" + closetList.ClosetCategoryPath[dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX] + "/map");
        Debug.Log("length : " + textures.Length);

        for (int i = 0; i < textures.Length; i++) {
            GameObject tmp = Instantiate(SkinButton, ScrollViewInfo.transform);
            Debug.Log("texture name : " + textures[i].name);
            Mesh tmp_mesh = Resources.Load<Mesh>("pcskin/" + closetList.ClosetCategoryPath[dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX] + "/fbx/" + textures[i].name);

            tmp.transform.name = "bt_skin_" + i;
            tmp.GetComponent<ClosetScrollInfoBt>().SetId(i);
            tmp.GetComponent<ClosetScrollInfoBt>().texture = textures[i];
            tmp.GetComponent<ClosetScrollInfoBt>().mesh = tmp_mesh;

            float x = -211.8f + 129.2f * (i % 4);
            float y = -129.2f * (int)(i / 4);

            tmp.transform.localPosition = new Vector3(x, y, 0);
            tmp.GetComponent<ClosetScrollInfoBt>().Refresh();
        }
    }
}
