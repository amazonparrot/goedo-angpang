﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetSkinInfoTable : MonoBehaviour {

    private LoadCSV _csv;

    public GameObject skinNameLabelObject;
    public GameObject skinInfoLabelObject;

    private UILabel nameLabel;
    private UILabel infoLabel;

    private ScriptLanguageMgr languageMgr = null;

    private bool isInited;

	// Use this for initialization
	void Start () {
	    _csv = GetComponent<LoadCSV>();

	    nameLabel = skinNameLabelObject.GetComponent<UILabel>();
	    infoLabel = skinInfoLabelObject.GetComponent<UILabel>();
        languageMgr = GameObject.Find("DataManager").GetComponent<ScriptLanguageMgr>();

	    nameLabel.text = "";
        infoLabel.text = "이야기가 궁금해? 조금만 기다려줘!";

    }
	
	// Update is called once per frame
	void Update () {
	    if (!isInited && _csv.isLoaded) {
	        isInited = true;

            FindSkinLine(GameObject.Find("DataManager").GetComponent<DataMgr>().playerTexture.name);
	    }
	}

    public void FindSkinLine(string textureName) {

        DataTable table = _csv.table;
        int ResultIndex = -1;

        for (int i = 1; i < table.line.Count; i++) {

            string result = table.Get(2, i);

            if (result == null) continue;

            if (textureName == result.Trim()) {
                ResultIndex = i;
                break;
            }

        }

        if (ResultIndex < 0) return;

        LANG lang = languageMgr.CurrentLanguage;
        int index = 3;

        switch (lang)
		{
			case LANG.JAPANESE:
				index = 7;
				break;
			case LANG.ENGLISH:
				index = 5;
				break;
		}

        nameLabel.text = table.Get(index, ResultIndex);
        infoLabel.text = table.Get(index + 1, ResultIndex);

    }

}
