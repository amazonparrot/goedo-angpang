﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetSlotBt : MonoBehaviour {

    public bool isNext;

    private DataMgr dataMgr;
    private ClosetScrollbarUI ScrollbarUI;


    // Use this for initialization
    void Start () {
	    dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        ScrollbarUI = GameObject.Find("ScrollView").GetComponent<ClosetScrollbarUI>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnClick() {

        int index = dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX;

        if (isNext) {
            index++;
        }
        else {
            index--;
        }

        if (index >= dataMgr.GetComponent<ClosetList>().ClosetCategory.Length) {
            return;
        }
        if (index < 0) {
            return;
        }

        dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX = index;
        dataMgr.SaveData();

        ScrollbarUI.Refresh();

    }
}
