﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetScrollInfoBt : MonoBehaviour {

    public Texture2D texture;
    public Mesh mesh;


    private Renderer playerRenderer;
    private Renderer buttonMeshRenderer;
    private GameObject player;
    private SkinnedMeshRenderer meshRenderer;
    private SkinnedMeshRenderer playerMeshRenderer;
    private UISprite background;

    private GameObject meshGameObject;

    private DataMgr dataMgr;
    private int id;

    private bool isUnlocked;

	// Use this for initialization
	void Start () {
	    id = 0;
	    player = GameObject.Find("PC_rig");
	    playerMeshRenderer = GameObject.Find("pc_mesh").GetComponent<SkinnedMeshRenderer>();
        playerRenderer = GameObject.Find("pc_mesh").GetComponent<Renderer>();
        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
	    isUnlocked = false;
	    // isUnlocked = true;

	    Refresh();


	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnClick() {

        if (!isUnlocked) return;

        playerRenderer.material.mainTexture = texture;
        playerMeshRenderer.sharedMesh = mesh;
        dataMgr.dataClass.CURRENT_PLAYER_SKIN = texture.name;
        dataMgr.dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX = dataMgr.dataClass.CURRENT_SKIN_SORT_INDEX;
        dataMgr.playerTexture = texture;
        dataMgr.playerMesh = mesh;
        dataMgr.SaveData();
        TweenRotation rotation = player.GetComponent<TweenRotation>();
        rotation.enabled = true;
        rotation.Reset();
        
        GameObject.Find("SkinDetailData").GetComponent<ClosetSkinInfoTable>().FindSkinLine(texture.name);
        GameObject.Find("bubble_info").GetComponent<ClosetBubbleInfo>().SetBubbleActive();
    }

    public void SetId(int _id) {
        id = _id;
    }

    public void Refresh() {

        for (int i = 0; i < transform.childCount; i++) {
            GameObject tmp = transform.GetChild(i).gameObject;

            if (tmp.transform.name == "mesh") {
                meshGameObject = tmp;
                buttonMeshRenderer = tmp.GetComponent<Renderer>();
                meshRenderer = tmp.GetComponent<SkinnedMeshRenderer>();
                continue;
            }

            if (tmp.transform.name == "Background") {
                background = tmp.GetComponent<UISprite>();
                continue;
            }
        }

        buttonMeshRenderer.material.mainTexture = texture;
        meshRenderer.sharedMesh = mesh;

        if (dataMgr == null) {
            dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        }

        for (int i = 0; i < dataMgr.dataClass.SKIN_UNLOCKED_LIST.Count; i++) {
            string tmp = dataMgr.dataClass.SKIN_UNLOCKED_LIST[i];
            if (texture == null) {
                break;
            }
            if (tmp == texture.name) {
                isUnlocked = true;
                break;
            }
        }

        if (!isUnlocked && meshGameObject.activeInHierarchy) {
            meshGameObject.SetActive(false);
            background.spriteName = "Skin_type_rock";
        }
        if (isUnlocked) {
            meshGameObject.SetActive(true);
            background.spriteName = "Skin_type_on";
        }

    }

}
