﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetTicketUI : MonoBehaviour {

    public GameObject LabelGameObject;
    public GameObject ParticleGameObject;

    public GameObject FadingPanel;

    public GameObject PanelGameObject;

    public int TicketCount = 1;

    private UILabel label;
    private ParticleSystem particle;
    private UIPanel panel;
    
    private ParticleSystem.MinMaxGradient color;
    private Color alphaColor;

    private bool isClosed;
    private float DeltaTime = 0;

	// Use this for initialization
	void Start () {
	    label = LabelGameObject.GetComponent<UILabel>();
	    particle = ParticleGameObject.GetComponent<ParticleSystem>();
	    color = particle.main.startColor;
	    panel = PanelGameObject.GetComponent<UIPanel>();
	    panel.alpha = 0;
		LANG lang = GameObject.Find("DataManager").GetComponent<ScriptLanguageMgr>().CurrentLanguage;

        alphaColor = new Color(1, 1, 1, 0);

		switch (lang)
		{
			case LANG.JAPANESE:
				label.text = "チケット : " + TicketCount;
				break;
			case LANG.ENGLISH:
				label.text = "Tickets : " + TicketCount;
				break;
			default:
				label.text = "뽑기권 : " + TicketCount;
				break;
		}
	    
	}
	
	// Update is called once per frame
	void Update () {

	    if (Input.GetMouseButtonUp(0)) {
	        isClosed = true;
	    }

	    if (isClosed) {

	        if (DeltaTime == 0) {
	            color.color = alphaColor;
	            var newMain = particle.main;
	            newMain.startColor = color;
	            if (FadingPanel != null) FadingPanel.SetActive(true);
	        }

	        DeltaTime += Time.deltaTime;

            if(panel.alpha > 0.01f)
	            panel.alpha = Mathf.Lerp(panel.alpha, 0, 0.1f);
            else {
                panel.alpha = 0;
            }

	    }
	    else {
	        if (panel.alpha < 0.99f) {
	            if (FadingPanel != null) FadingPanel.SetActive(false);
                panel.alpha = Mathf.Lerp(panel.alpha, 1, 0.2f);
	        }
	        else {
	            panel.alpha = 1;
            }
        }

        if(FadingPanel != null)
	        FadingPanel.GetComponent<UIPanel>().alpha = 1 - panel.alpha;

	    if (DeltaTime > 2) {
	        Destroy(gameObject);
	        return;
	    }

	}

    public void SetFadingPanel(GameObject _gameObject) {
        FadingPanel = _gameObject;
    }
}
