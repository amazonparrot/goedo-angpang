﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToLobyBt : MonoBehaviour {
    // Use this for initialization

    public bool isNeedCutScene;

    private void Start() {
    }

    // Update is called once per frame
    private void Update() {
    }

    private void OnClick() {
        if (SceneManager.GetActiveScene().name == "InGameScene") {
            var dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();

            if (dataMgr.dataClass.CURRENT_STAGE_ID + 1 <
                dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SIZE)
                StartCoroutine(DataMgr.SetScene("MainScene"));
            else if(isNeedCutScene) StartCoroutine(DataMgr.SetScene("CartoonScene"));
            else {
                StartCoroutine(DataMgr.SetScene("MainScene"));
            }
        }
        else
            StartCoroutine(DataMgr.SetScene("MainScene"));
    }
}