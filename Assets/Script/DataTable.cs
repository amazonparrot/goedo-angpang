﻿
using System.Collections.Generic;

public class DataTableLine {

    public List<string> node;

    public DataTableLine() {
        node = new List<string>();
    }

    public DataTableLine(int col) {
        node = new List<string>();

        for (int i = 0; i < col; i++) {
            node.Add(null);
        }
    }

}

public class DataTable {

    public List<DataTableLine> line;

    public DataTable() {
        line = new List<DataTableLine>();
    }

    public DataTable(int col) {
        line = new List<DataTableLine>();

        for (int i = 0; i < col; i++) {
            line.Add(new DataTableLine());
        }

    }

    public DataTable(int col, int row) {
        line = new List<DataTableLine>();

        for (int i = 0; i < col; i++) {
            line.Add(new DataTableLine(row));
        }
    }

    public void Insert(string value, int row, int col) {
        if (col > line.Count - 1) {
            for (int i = 0; i < col - (line.Count - 1); i++) {
                line.Add(new DataTableLine());
            }
        }

        if (row > line[col].node.Count - 1) {
            for (int i = 0; i < row - (line[col].node.Count - 1); i++) {
                line[col].node.Add(null);
            }
        }

        line[col].node[row] = value;

    }

    public string Get(int row, int col) {

        if ((col > line.Count - 1) || (row > line[col].node.Count - 1)) {
            return null;
        }

        return line[col].node[row];
    }

}
