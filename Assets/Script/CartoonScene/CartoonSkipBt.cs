﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartoonSkipBt : MonoBehaviour {

    private CartoonManager cartoonManager;

	// Use this for initialization
	void Start () {
	    cartoonManager = GameObject.Find("CartoonManager").GetComponent<CartoonManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnClick() {
        cartoonManager.OnSkip();
    }
}
