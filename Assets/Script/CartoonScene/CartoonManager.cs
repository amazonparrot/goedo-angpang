﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CartoonManager : MonoBehaviour {

    public GameObject MainCameraGameObject;
    private Camera MainCamera;

    public GameObject TouchGameObject;

    private AudioSource audioSource;

    private GameObject CartoonObjects;
    private int CutCurrent;

    private int CutMaximum = -1;

    private DataMgr dataMgr;
    public GameObject[] EndingCutScenes;
    private GameObject[] objects;

    public GameObject[] OpeningCutScenes;

    // Use this for initialization
    private void Start() {
        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        audioSource = GetComponent<AudioSource>();
        audioSource.mute = dataMgr.dataClass.CURRENT_MUTE;

        if (MainCamera && Screen.height > 0) {
            var orgRatio = 1280f / 720f;
            var ratio = (float)Screen.width / (float)Screen.height;
            if (ratio > 0) {
                MainCamera.backgroundColor = Color.black;
                MainCamera.orthographicSize = orgRatio / ratio;
            }
        }


        var tmps = dataMgr.dataClass.CURRENT_STAGE_ID == 0 ? OpeningCutScenes : EndingCutScenes;
        CartoonObjects = tmps[dataMgr.dataClass.CURRENT_EPISODE_ID];
        CutMaximum = CartoonObjects.transform.childCount;

        CartoonObjects.SetActive(true);

        if (CutMaximum < 0) return;

        objects = new GameObject[CutMaximum];

        for (var i = 0; i < CutMaximum; i++) {
            objects[i] = CartoonObjects.transform.GetChild(i).gameObject;
            CartoonObjects.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    private void Update() {
        if (CutMaximum < 0) return;

        if (Input.GetMouseButtonUp(0) && (CutCurrent < CutMaximum)) {
            audioSource.Play();
            if (CutCurrent <= 0) {
                TouchGameObject.GetComponent<TweenAlpha>().enabled = true;
            }
            objects[CutCurrent].SetActive(true);
            CutCurrent++;
        }
        else if (Input.GetMouseButtonUp(0)) {
            OnSkip();
        }

        if (Application.platform == RuntimePlatform.Android) {

            if (Input.GetKeyUp(KeyCode.Escape)) {

                OnSkip();

            }

        }
    }

    public void OnSkip() {
        StartCoroutine(DataMgr.SetScene(dataMgr.dataClass.CURRENT_STAGE_ID == 0 ? "InGameScene" : "MainScene"));
    }
}