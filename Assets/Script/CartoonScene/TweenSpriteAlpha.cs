﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenSpriteAlpha : UITweener {

    public float from = 1f;
    public float to = 1f;

    private SpriteRenderer mRender;

    public float alpha {
        get {
            if (mRender != null) return mRender.color.a;
            return 0f;
        }
        set {
            if (mRender != null)
                mRender.color = new Color(mRender.color.r, mRender.color.g, mRender.color.b, value);
        }
    }

    // Use this for initialization
    void Awake() {
        mRender = GetComponent<SpriteRenderer>();
    }


    protected override void OnUpdate(float factor, bool isFinished) {
        alpha = Mathf.Lerp(from, to, factor);
    }

    static public TweenSpriteAlpha Begin(GameObject go, float duration, float alpha) {
        TweenSpriteAlpha comp = UITweener.Begin<TweenSpriteAlpha>(go, duration);
        comp.from = comp.alpha;
        comp.to = alpha;

        if (duration <= 0f) {
            comp.Sample(1f, true);
            comp.enabled = false;
        }
        return comp;
    }
}
