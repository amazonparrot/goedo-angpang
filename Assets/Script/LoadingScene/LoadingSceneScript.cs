﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneScript : MonoBehaviour {

    public GameObject LoadingObject;
    public GameObject DataManager = null;


    private bool isLoaded;

    public LoadingSceneScript() {
    }

    void Awake() {
        GameObject obj = GameObject.FindWithTag("Data");

        if (obj == null) {
            obj = Instantiate(DataManager);
            obj.transform.name = "DataManager";
        }
        //StartCoroutine(Loading());
        DataMgr.SetUIRootScaleStyle();
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		SceneManager.LoadSceneAsync("IntroScene");
	}

    public IEnumerator Loading() {
        if (LoadingObject == null) yield return null;

        GameObject _gameObject = Instantiate(LoadingObject);

        yield return _gameObject;

    }
}
