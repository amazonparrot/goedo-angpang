﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LenguageSelectLabel : MonoBehaviour {

public string StringJPN;
	public string StringENG;
	private string StringKOR;

	private UILabel label = null;
	private ScriptLanguageMgr languageMgr = null;
	private LANG CurrentLanguage;


	// Use this for initialization
	void Start () {
		label = GetComponent<UILabel>();
		languageMgr = GameObject.Find("DataManager").GetComponent<ScriptLanguageMgr>();
		StringKOR = label.text;

		SetLanguage();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetLanguage() {
		CurrentLanguage = languageMgr.CurrentLanguage;

		switch (CurrentLanguage)
		{
			case LANG.JAPANESE:
				label.text = StringJPN;
				break;
			case LANG.ENGLISH:
				label.text = StringENG;
				break;
			default:
				label.text = StringKOR;
				break;
		}
	}
}

