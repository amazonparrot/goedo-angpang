﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class CameraMgr : MonoBehaviour {

    public GameObject MainCamera = null;
    public GameObject DarkSprite;
    private TweenSpriteAlpha spriteAlpha;
    public Vector2 Rotation;
    public float FinalRotationY = 0f;
    public float Sensitivity = 2f;

    public GameObject GameSystemManager;

    public bool isRelax = true;

    private ColorCorrectionCurves colorCorrection;
    private float grayScale {
        get { return colorCorrection.saturation; }

        set {
            colorCorrection.saturation = value;
            BGM_AudioSource.pitch = value / 4 + 0.75f;
        }
    }

    private AudioSource BGM_AudioSource;

    //private Camera mainCamera;

    private Quaternion quaternion;
    public Vector2 mouse_delta;

    private GameSystemMgr gameSystemMgr;
    private TileMgr tileMgr;
    private StageMgr stageMgr;
    

    // Use this for initialization
    void Start() {
        gameSystemMgr = GameSystemManager.GetComponent<GameSystemMgr>();
        tileMgr = GameObject.Find("TileManager").GetComponent<TileMgr>();
        colorCorrection = MainCamera.GetComponent<ColorCorrectionCurves>();
        BGM_AudioSource = GameObject.Find("DataManager").GetComponent<AudioSource>();
        spriteAlpha = DarkSprite.GetComponent<TweenSpriteAlpha>();
        spriteAlpha.alpha = 0;

        stageMgr = GameObject.Find("StageManager").GetComponent<StageMgr>();

        Init();
    }

    public void Init() {
        quaternion = Quaternion.identity;
        Rotation = new Vector2(0, 0);
        mouse_delta = new Vector2(0, 0);
        //accrueResultTime = 0;
        //accrueStartResultTime = -99;
    }

    // Update is called once per frame
    void Update() {
        if (!tileMgr.isLoad) return;

        if (gameSystemMgr.isCleared) {
            BGM_AudioSource.volume = Mathf.Lerp(BGM_AudioSource.volume, 0, 0.1f);
        }


        if (gameSystemMgr.isPortalArrived || gameSystemMgr.isFailed || gameSystemMgr.isPause) {

            if (spriteAlpha.alpha <= 0) {


                spriteAlpha.enabled = true;
                spriteAlpha.@from = 0;
                spriteAlpha.to = 1;
                spriteAlpha.Reset();


            }

            quaternion.eulerAngles = new Vector3(0, FinalRotationY, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, quaternion, Time.deltaTime * 15);

            if (!isRelax) {
                isRelax = Math.Abs(transform.eulerAngles.x) < 0.0001f && Math.Abs(transform.eulerAngles.z) < 0.0001f;
            }

            if (gameSystemMgr.isFailed) {
                BGM_AudioSource.pitch = Mathf.Lerp(BGM_AudioSource.pitch, 0, 0.1f);
            }

            return;
        }

        if (!gameSystemMgr.isPause) {

            if (spriteAlpha.alpha >= 1) {
                spriteAlpha.enabled = true;
                spriteAlpha.@from = 1;
                spriteAlpha.to = 0;
                spriteAlpha.Reset();

            }

        }

        if (!gameSystemMgr.isPlayerIdle || stageMgr.isPortalUsing) {
            quaternion.eulerAngles = new Vector3(0, FinalRotationY, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, quaternion, Time.deltaTime * 15);

            if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)) {
                Rotation.Set(0, 0);
                mouse_delta.Set(-Input.mousePosition.y / Sensitivity, Input.mousePosition.x / Sensitivity);
            }

            return;
        }

        if (Input.GetMouseButtonDown(0)) {
            Rotation.Set(0, 0);
            mouse_delta.Set(-Input.mousePosition.y / Sensitivity, Input.mousePosition.x / Sensitivity);
        }

        if (Input.GetMouseButton(0)) {

            Rotation.x += -Input.mousePosition.y / Sensitivity - mouse_delta.x;
            Rotation.y += Input.mousePosition.x / Sensitivity - mouse_delta.y;
            mouse_delta.Set(-Input.mousePosition.y / Sensitivity, Input.mousePosition.x / Sensitivity);

            if (Rotation.x < 0) {
                Rotation.x = 0;
            }else if (Rotation.x > 90) {
                Rotation.x = 90;
            }

            if (Math.Abs(Rotation.y) < 15f && isRelax) {

                if (Math.Abs(Rotation.x) > 15f) {
                    isRelax = false;

                    quaternion.eulerAngles = new Vector3(Rotation.x, FinalRotationY, 0);
                    transform.rotation = Quaternion.Lerp(transform.rotation, quaternion, Time.deltaTime * 10);
                }
                else {
                    isRelax = true;
                }

                return;
            }
            isRelax = false;

            quaternion.eulerAngles = new Vector3(Rotation.x, FinalRotationY + Rotation.y, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, quaternion, Time.deltaTime * 10);

        }else if (Input.GetMouseButtonUp(0)) {

            if (Math.Abs(Rotation.y) < 15f)
                return;
                // ReSharper disable once PossibleLossOfFraction
                FinalRotationY = (int)(FinalRotationY + Rotation.y + (FinalRotationY + Rotation.y > 0 ? 45 : -45))/90*90f;

            if (Math.Abs(FinalRotationY) == 360) {
                FinalRotationY = 0;
            }else if (Math.Abs(FinalRotationY) == 270) {
                FinalRotationY = (FinalRotationY > 0) ? -90 : 90;
            }else if (FinalRotationY == -180) {
                FinalRotationY = 180;
            }
        }
        else {
            quaternion.eulerAngles = new Vector3(0, FinalRotationY, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, quaternion, Time.deltaTime * 15);

            if (!isRelax) {
                isRelax = Math.Abs(transform.eulerAngles.x) < 0.0001f && Math.Abs(transform.eulerAngles.z) < 0.0001f;
            }
        }

        //화면 흑백
        if (isRelax && grayScale < 1) {

            grayScale = Mathf.Lerp(grayScale, 1, 0.1f);

            if (grayScale > 0.99) {
                grayScale = 1;
            }
        }else if (!isRelax && grayScale > 0.4) {
            grayScale = Mathf.Lerp(grayScale, 0.4f, 0.1f);

            if (grayScale < 0.41) {
                grayScale = 0.4f;
            }
        }
    }

    public void ResetRotation() {
        FinalRotationY = 0;
        isRelax = false;
        //transform.rotation = Quaternion.identity;
        //quaternion = Quaternion.identity;

    }

}