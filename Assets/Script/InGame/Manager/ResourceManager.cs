﻿using UnityEngine;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour {
    public GameObject AwesomeTile = null;

    public GameObject BackgroundObject;

    protected DataMgr dataMgr;
    public GameObject FireTile = null;
    public GameObject GoalTile = null;

    //타일 종류 (꼭 유지보수 가능한 쪽으로 바꿉시다 제발 나님아/........)
    public GameObject GrassTile = null;
    public GameObject NomalTile = null;
    public GameObject PortalTile = null;
    public GameObject SpawnTile = null;
    public GameObject VanishTile = null;


    private void awake() {
    }

    // Use this for initialization
    private void Start() {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        Load();
    }

    public void Load() {
        var episodeId = dataMgr.dataClass.CURRENT_EPISODE_ID + 1;
        var stageId = SetStageId(dataMgr.dataClass.CURRENT_STAGE_ID + 1);


        //BackgroundObject.GetComponent<RawImage>().texture = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Textures/Bg/stage" + stageID + "_background.png");
        BackgroundObject.GetComponent<RawImage>().texture =
            Resources.Load<Texture2D>("Bg/episode" + episodeId + "/stage" + stageId + "_background");

        var tileRenderer = new Renderer[8];

        tileRenderer[0] = GrassTile.GetComponent<Renderer>();
        tileRenderer[1] = NomalTile.GetComponent<Renderer>();
        tileRenderer[2] = AwesomeTile.GetComponent<Renderer>();
        tileRenderer[3] = FireTile.GetComponent<Renderer>();
        tileRenderer[4] = SpawnTile.GetComponent<Renderer>();
        tileRenderer[5] = GoalTile.GetComponent<Renderer>();
        tileRenderer[6] = PortalTile.GetComponent<Renderer>();
        tileRenderer[7] = VanishTile.GetComponent<Renderer>();

        Texture2D[] textures;
        textures = Resources.LoadAll<Texture2D>("Tile/episode" + episodeId + "/Stage_0" + stageId);
        Debug.Log("textures count : " + textures.Length);

        for (var i = 0; i < textures.Length; i++) {
            //Texture2D tmp = AssetDatabase.LoadAssetAtPath<Texture2D>(objs[i]);
            var tmp = textures[i];

            switch (tmp.name.Split('_')[0]) {
                case "GrassTile":
                    tileRenderer[0].material.mainTexture = tmp;
                    break;
                case "NomalTile":
                case "TileBase":
                    tileRenderer[1].material.mainTexture = tmp;
                    break;
                case "AwesomeTile":
                    tileRenderer[2].material.mainTexture = tmp;
                    break;
                case "FireTile":
                    tileRenderer[3].material.mainTexture = tmp;
                    break;
                case "SpawnTile":
                    tileRenderer[4].material.mainTexture = tmp;
                    break;
                case "GoalTile":
                    tileRenderer[5].material.mainTexture = tmp;
                    break;
                case "PortalTile":
                    tileRenderer[6].material.mainTexture = tmp;
                    break;
                case "VanishTile":
                    tileRenderer[7].material.mainTexture = tmp;
                    break;
            }
        }
    }

    public static int SetStageId(int stageId) {
        var episodeId = GameObject.FindWithTag("Data").GetComponent<DataMgr>().dataClass.CURRENT_EPISODE_ID + 1;

        if (episodeId == 1) {
            if (stageId >= 11) return 3;
            if (stageId >= 6) return 2;
            return 1;
        }
        return 1;
    }
}