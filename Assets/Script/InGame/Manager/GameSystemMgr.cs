﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class GameSystemMgr : MonoBehaviour {

    public GameObject PlayerObject;
    public GameObject TileManager;
    public GameObject ticketPopUp;
    public GameObject CannotAccessObject;

    public bool isAnalyticsEnabled = true;

    public bool isPortalArrived;
    public bool isCleared;
    public bool isFailed;
    public bool isPause;
    private bool isEnd;

    private TileMgr tileMgr;
    private CameraMgr cameraMgr;
    private APlayer Player;
    private DataMgr dataMgr;
    private StageMgr stageMgr;

    public bool isPlayerMoving;
    public bool isPlayerMovingUp;
    public bool isPlayerIdle;
    public bool isPlayerStageNeedUpdate;

    public int m_playerMovingCount;
    public int m_nextStageId;

    public int[] m_playerMovementMaximum;
    public int m_playerScore;

    public int m_switchMaximum;
    public int m_switchCount;

    public bool isNpcOrdered;
    private bool isSelected;

    private Vector3 selectedTilePosition;
    private Vector3 prevPlayerPosition;
    private Vector3 lerpedPosition;
    private float prevPlayerStartTime;
    private float prevPlayerMoveDelay;

    private Vector3 mapLength;

    private float MovingTime;

    private float PlayTime;

	// Use this for initialization
	protected void Start () {
	    tileMgr = TileManager.GetComponent<TileMgr>();
	    Player = PlayerObject.GetComponent<APlayer>();
	    cameraMgr = tileMgr.CameraManager.GetComponent<CameraMgr>();
	    stageMgr = tileMgr.StageManager.GetComponent<StageMgr>();
        mapLength = Vector3.zero;
	    m_playerScore = 3;
	    m_switchMaximum = 0;
	    m_switchCount = 0;

        m_playerMovementMaximum = new int[2];

        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
	    dataMgr.dataClass.CURRENT_NOT_TUTORIAL = true;
        dataMgr.SaveData();

	    PlayTime = 0;
	    isPlayerIdle = true;


        Init();
	}

    public void Init() {
        isPortalArrived = false;
        isFailed = false;
        isPause = false;
        isCleared = false;
        isPlayerStageNeedUpdate = false;
        isEnd = false;
        m_playerMovingCount = 0;

        MovingTime = 1;

        GameObject textGameObject = GameObject.Find("ingame_accrue_move_count");
        if (textGameObject != null) {
            UILabel textObj = textGameObject.GetComponent<UILabel>();
            if (textObj != null) textObj.text = "0";
        }
    }
	
	// Update is called once per frame
	protected void Update () {

	    PlayTime += Time.deltaTime;

	    if (!tileMgr.isLoad) return;
        

	    if (isPause) return;
	    if (isCleared) {
	        if (!isEnd) {
                Player.animator.SetBool("isWin", true);
                isEnd = true;

	            bool isNewRaiting = false;

                int currentEpisode = dataMgr.dataClass.CURRENT_EPISODE_ID;
                int currentStage = dataMgr.dataClass.CURRENT_STAGE_ID;

                if (currentStage >= dataMgr.dataClass.EPISODE_DATA[currentEpisode].STAGE_SIZE - 1) {
                    currentStage = 0;
                    currentEpisode = currentEpisode + 1 > dataMgr.dataClass.EPISODE_SIZE - 1 ? 0 : currentEpisode + 1;
                }
                else {
                    currentStage++;
                }
                
                dataMgr.dataClass.EPISODE_DATA[currentEpisode].STAGE_UNLOCK[currentStage] = true;

                if (
	                dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SCORE[
	                    dataMgr.dataClass.CURRENT_STAGE_ID] < m_playerScore) {
	                dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SCORE[
	                    dataMgr.dataClass.CURRENT_STAGE_ID] = m_playerScore;

                    isNewRaiting = true;

                    int currentScore = 0;
                    for (int i = 0; i < dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SIZE; i++) {
                        currentScore += dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SCORE[i];
                    }

	                bool isReward = false;
	                int reward = dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_REWARD;

	                if (currentScore >= 45) {
                        if(reward < 3)
	                        isReward = true;
                    }
                    else if (currentScore >= 30) {
                        if(reward < 2)
	                        isReward = true;
                    }
                    else if (currentScore >= 15) {
                        if(reward < 1)
	                        isReward = true;
	                }

	                if (isReward) {
	                    dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_REWARD++;
                        int ticketLength = reward + 1;
	                    dataMgr.dataClass.CURRENT_LOTTO_TICKET += ticketLength;

	                    var _obj = Instantiate(ticketPopUp, GameObject.Find("Anchor").transform);
	                    _obj.GetComponent<GetTicketUI>().TicketCount = ticketLength;
                        dataMgr.SaveData();
	                }

                }

	            if (isAnalyticsEnabled) {
                    Analytics.CustomEvent("ClearTime", new Dictionary<string, object> {
                    { "Stage", (dataMgr.dataClass.CURRENT_EPISODE_ID * 15 + dataMgr.dataClass.CURRENT_STAGE_ID + 1) },
                    { "ElapsedTime", PlayTime },
                    { "Rating", (dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_REWARD + 1) },
                    { "isNewRaiting", isNewRaiting ? "true" : "false" },
                    { "Version", Application.version },
                    { "EpisodeID", dataMgr.dataClass.CURRENT_EPISODE_ID + 1 }
                });
                }
                
            }
	        return;
	    }

        if (isFailed && !Player.animator.GetBool("isArrest")) {
            Player.animator.SetBool("isArrest", true);
            return;
        }

        if (isPortalArrived) {
            stageMgr.SetStageChanged(m_nextStageId);
        }

        if (isPlayerMoving) {
            isPlayerIdle = false;
            OnPlayerMove();
	        return;
	    }

        if (tileMgr.isClicked && isPlayerIdle) {
            isSelected = OnSelected();
            if (!isSelected) tileMgr.isClicked = false;
        }


    }

    void LateUpdate() {

        if (tileMgr.isClicked && isSelected) {
            tileMgr.isClicked = false;
            isSelected = false;
            if(!isPortalArrived)
                isNpcOrdered = true;
        }else if (isNpcOrdered) {
            isNpcOrdered = false;
        }

        if (isPlayerMovingUp) {
            Player.animator.SetBool("isRun", false);
            Player.animator.SetBool("isLand", false);
            Player.animator.SetBool("isJump", false);
            isPlayerMovingUp = false;
            Player.isPlayerMoving = false;

        }


        if (prevPlayerStartTime > MovingTime) {
            prevPlayerStartTime = 0;
            prevPlayerMoveDelay = 0;
            isPlayerStageNeedUpdate = false;
            isPlayerMovingUp = true;
            isPlayerMoving = false;
            Player.transform.position = selectedTilePosition;
        }

        prevPlayerMoveDelay += Time.deltaTime;
        if (!isPlayerMoving && prevPlayerMoveDelay > 0.2f) {
            isPlayerIdle = true;
        }

        if (isPlayerStageNeedUpdate) {
            isPlayerStageNeedUpdate = false;
            prevPlayerStartTime = 0;
            isPlayerMovingUp = true;
            isPlayerMoving = false;
        }
    }

    public void SetPlayerMovingUp(bool enable) {
        isPlayerStageNeedUpdate = enable;
    }

    public bool isFrontFunction() {
        int rotationY = (int)cameraMgr.FinalRotationY;
        switch (rotationY) {
            case 0:
            case 90:
                return true;
                
        }
        return false;
    }

    public bool isPositionXFunction() {
        int rotationY = (int)cameraMgr.FinalRotationY;
        return Math.Abs(rotationY) != 90;
    }

    private bool OnSelected() {
        if (tileMgr.GetClickedTileVector3() == -Vector3.one) return false;
        if (Player.positionId == tileMgr.GetClickedTileVector3()) return false;
        if (stageMgr.isPortalUsing || isPortalArrived) return false;
        if (Player.isBlocked) {
            Player.SetSpeechBubble("앞이 보이지 않아!");
            return false;
        }

        if (!OnJudgePlayerMovable(tileMgr.GetClickedTileVector3())) {

            SetCannotAccessSign(tileMgr.GetClickedTile().transform.position);

            return false;
        }

        m_playerMovingCount++;

        selectedTilePosition = tileMgr.GetClickedTile().transform.position + new Vector3(0, 0.5f, 0);
        Player.positionId = tileMgr.GetClickedTileVector3();
        Player.Position = Id2Position(Player.positionId) + new Vector3(0, 0.5f, 0);
        prevPlayerPosition = Player.transform.position;
        prevPlayerStartTime = 0;
        isPlayerMoving = true;
        Player.isPlayerMoving = true;

        bool isFront = false;
        switch ((int)cameraMgr.FinalRotationY) {
            case 0:
            case 90:
                isFront = true;
                break;
            case 180:
            case -90:
                isFront = false;
                break;
        }

        if (Math.Abs((int)cameraMgr.FinalRotationY) != 90) {
            bool tmp = Player.transform.position.x - selectedTilePosition.x > 0;

            if (!isFront) {
                lerpedPosition = new Vector3((tmp
                    ? 1
                    : -1), 0, tmp ? 0 : 2);
            }
            else {
                lerpedPosition = new Vector3((tmp
                                 ? 1
                                 : -1), 0, tmp ? -2 : 0);
            }
        }
        else {
            bool tmp = Player.transform.position.z - selectedTilePosition.z > 0;

            if (!isFront) {
                lerpedPosition = new Vector3(tmp ? 2 : 0, 0, (tmp
                                 ? 1
                                 : -1));
            }
            else {
                lerpedPosition = new Vector3(tmp ? 0 : -2, 0, (tmp
                                 ? 1
                                 : -1));
            }
            
        }
        var MoveCountObject = GameObject.Find("ingame_accrue_move_count");
        if (MoveCountObject != null) MoveCountObject.GetComponent<GaugeMoveCount>().SetCountValue(m_playerMovingCount);
        return true;
    }

    public bool OnJudgeActorMovable(Vector3 tileId, AActor actor) {
        Vector3 clickedTileId = tileId;
        int rotationY = (int)cameraMgr.FinalRotationY;
        int heightLevel = (int)(clickedTileId.y - actor.positionId.y + 1);
        bool isPositionX = Math.Abs(rotationY) != 90;

        bool isFront = false;

        switch (rotationY) {
            case 0:
            case 90:
                isFront = true;
                break;
            case 180:
            case -90:
                isFront = false;
                break;
        }

        if (mapLength == Vector3.zero) {
            mapLength = tileMgr.GetIdLength();
        }

        GameObject tmpTile = null;

        //클릭한 타일 위에 다른 타일이 있을 경우 예외처리
        if (clickedTileId.y < mapLength.y - 1) {
            tmpTile = tileMgr.GetSurfaceTile(isPositionX, isFront, clickedTileId + Vector3.up);
            if (tmpTile != null) {
                if (!tmpTile.GetComponent<TileObject>().IsCanPlayerIgnoreBlock())
                    return false;
                if (tmpTile.GetComponent<AttackTile>() != null) {
                    if (!tmpTile.GetComponent<AttackTile>().isBehindEmpty()) {
                        return false;
                    }
                }
            }
        }

        //무시하는 타일의 예외처리
        if (clickedTileId.y < mapLength.y - 1) {
            tmpTile = tileMgr.GetSurfaceTile(isPositionX, isFront, clickedTileId);
            if (tmpTile != null) {
                if (tmpTile.GetComponent<TileObject>().IsCanPlayerIgnoreBlock())
                    return false;
            }
        }

        if (heightLevel == 0) {

            float gap = Math.Abs(clickedTileId.x - actor.positionId.x);



            //x축 기준 조건이 맞는 경우
            if (isPositionX && gap <= 1 && gap > 0) {
                actor.ResetAnimationDeltaTime();
                return true;
            }

            gap = Math.Abs(clickedTileId.z - actor.positionId.z);
            //z축 기준 조건이 맞는 경우
            if (!isPositionX && gap <= 1 && gap > 0) {
                actor.ResetAnimationDeltaTime();
                return true;
            }
            //조건에 만족하지 않는 경우
            else {
                return false;
            }
        }

        return false;
    }

    private bool OnJudgePlayerMovable(Vector3 tileId) {
        Vector3 clickedTileId = tileId;
        int rotationY = (int) cameraMgr.FinalRotationY;
        int heightLevel = (int) (clickedTileId.y - Player.positionId.y);
        bool isPositionX = Math.Abs(rotationY) != 90;

        bool isFront = false;

        switch (rotationY) {
            case 0:
            case 90:
                isFront = true;
                break;
            case 180:
            case -90:
                isFront = false;
                break;
        }

        if (mapLength == Vector3.zero) {
            mapLength = tileMgr.GetIdLength();
        }


        GameObject tmpTile = null;

        //클릭한 타일 위에 다른 타일이 있을 경우 예외처리
        if (clickedTileId.y < mapLength.y - 1) {
            tmpTile = tileMgr.GetSurfaceTile(isPositionX, isFront, clickedTileId + Vector3.up);
            if (tmpTile != null) {
                if (!tmpTile.GetComponent<TileObject>().IsCanPlayerIgnoreBlock())
                    return false;
                if (tmpTile.GetComponent<AttackTile>() != null) {
                    if (!tmpTile.GetComponent<AttackTile>().isBehindEmpty()) {
                        return false;
                    }
                }
            }
        }

        //무시하는 타일의 예외처리
        if (clickedTileId.y < mapLength.y - 1) {
            tmpTile = tileMgr.GetSurfaceTile(isPositionX, isFront, clickedTileId);
            if (tmpTile != null) {
                var tmpTileObj = tmpTile.GetComponent<TileObject>();
                if (tmpTileObj.IsCanPlayerIgnoreBlock())
                    return false;
                if (tmpTileObj.isNotAccess) {
                    return false;
                }
            }
        }



        //플레이어 위
        if (heightLevel == 1) {
            Vector3 tempTile = Player.positionId + new Vector3(0, 2, 0);
            //플레이어 바로 위에 천장이 없을 경우
            if (tempTile.y > mapLength.y - 1 || tileMgr.GetSurfaceTile(isPositionX, isFront, tempTile) == null) {

                //플레이어가 벽에 막혀있는데 올라가려고 할 경우
                if (tempTile.y <= mapLength.y - 1) {
                    tmpTile = tileMgr.GetSurfaceTile(isPositionX, isFront, Player.positionId + new Vector3(0, 1, 0));
                    if (tmpTile != null) {
                        if (!tmpTile.GetComponent<TileObject>().IsCanPlayerIgnoreBlock())
                            return false;
                    }
                }
                

                //x축 기준 조건이 맞는 경우
                if (isPositionX && Math.Abs(clickedTileId.x - Player.positionId.x) == 1) {
                    Player.animator.SetBool("isJump", true);
                    Player.ResetAnimationDeltaTime();
                    return true;
                }
                //z축 기준 조건이 맞는 경우
                else if (!isPositionX && Math.Abs(clickedTileId.z - Player.positionId.z) == 1) {
                    Player.ResetAnimationDeltaTime();
                    Player.animator.SetBool("isJump", true);
                    return true;
                }
                //조건에 만족하지 않는 경우
                else {
                    return false;
                }
            }
        //플레이어 중심
        }else if (heightLevel == 0) {

            float gap = Math.Abs(clickedTileId.x - Player.positionId.x);

            //x축 기준 조건이 맞는 경우
            if (isPositionX && gap <= 1 && gap > 0) {
                Player.ResetAnimationDeltaTime();
                Player.animator.SetBool("isRun", true);
                return true;
            }

            gap = Math.Abs(clickedTileId.z - Player.positionId.z);
            //z축 기준 조건이 맞는 경우
            if (!isPositionX && gap <= 1 && gap > 0) {
                Player.ResetAnimationDeltaTime();
                Player.animator.SetBool("isRun", true);
                return true;
            }
            //조건에 만족하지 않는 경우
            else {
                return false;
            }
        }
        //플레이어 아래
        else if(heightLevel == -1) {
            Vector3 tempTile = clickedTileId + new Vector3(0, 2, 0);

            //플레이어 옆이 제한 높이보다 높을 경우
            if (tempTile.y > mapLength.y - 1) {
                //x축 기준 조건이 맞는 경우
                if (isPositionX && Math.Abs(clickedTileId.x - Player.positionId.x) == 1) {
                    Player.ResetAnimationDeltaTime();
                    Player.animator.SetBool("isLand", true);
                    return true;
                }
                //z축 기준 조건이 맞는 경우
                if (!isPositionX && Math.Abs(clickedTileId.z - Player.positionId.z) == 1) {
                    Player.ResetAnimationDeltaTime();
                    Player.animator.SetBool("isLand", true);
                    return true;
                }
                //조건에 만족하지 않는 경우
                return false;
            }

            //플레이어 옆에 있는 장애물이 존재하지 않을 경우
            tmpTile = tileMgr.GetSurfaceTile(isPositionX, isFront, tempTile);
            if (tempTile.y > mapLength.y - 1 || tmpTile == null || tmpTile.GetComponent<TileObject>().IsCanPlayerIgnoreBlock()) {
                //x축 기준 조건이 맞는 경우
                if (isPositionX && Math.Abs(clickedTileId.x - Player.positionId.x) == 1) {
                    Player.ResetAnimationDeltaTime();
                    Player.animator.SetBool("isLand", true);
                    return true;
                }
                //z축 기준 조건이 맞는 경우
                else if (!isPositionX && Math.Abs(clickedTileId.z - Player.positionId.z) == 1) {
                    Player.ResetAnimationDeltaTime();
                    Player.animator.SetBool("isLand", true);
                    return true;
                }
                //조건에 만족하지 않는 경우
                else {
                    return false;
                }
            }

        }
        

        return false;
    }

    private void OnPlayerMove() {

        if (isFailed) return;

        Vector3 playerPos = prevPlayerPosition;
        prevPlayerStartTime += Time.deltaTime * 1.5f;

        bool run = Player.animator.GetBool("isRun");
        bool land = Player.animator.GetBool("isLand");
        bool jump = Player.animator.GetBool("isJump");
        bool isPositionX = Math.Abs((int)cameraMgr.FinalRotationY) != 90;


        Vector3 dir = Player.transform.position - selectedTilePosition;
        Quaternion lookRot = Quaternion.LookRotation(lerpedPosition);
        lookRot.x = 0; lookRot.z = 0;
        Player.transform.rotation = Quaternion.Slerp(Player.transform.rotation, lookRot, 0.1f);

        

        if (jump) {
            //float remakedDeltaTime = Mathf.Cos(prevPlayerStartTime*Mathf.PI/2 + Mathf.PI) + 1;
            float remakedDeltaTime = prevPlayerStartTime;
            Player.transform.position = new Vector3(Mathf.Lerp(playerPos.x, selectedTilePosition.x, remakedDeltaTime),
            Mathf.Lerp(playerPos.y, playerPos.y + ( -(1 / (9 * (prevPlayerStartTime + 0.073f))) + 1.1f) + Mathf.Sin(prevPlayerStartTime * Mathf.PI) * 0.1f, 1),
            Mathf.Lerp(playerPos.z, selectedTilePosition.z, prevPlayerStartTime));
        }else if (land) {
            Player.transform.position = new Vector3(Mathf.Lerp(playerPos.x, selectedTilePosition.x * Mathf.Sin(prevPlayerStartTime / 2 * Mathf.PI), prevPlayerStartTime),
            Mathf.Lerp(playerPos.y, selectedTilePosition.y + Mathf.Sin(prevPlayerStartTime * Mathf.PI) * 1.5f, prevPlayerStartTime),
            Mathf.Lerp(playerPos.z, selectedTilePosition.z * Mathf.Sin(prevPlayerStartTime / 2 * Mathf.PI), prevPlayerStartTime));
        }
        else if(run){
            Player.transform.position = new Vector3(Mathf.Lerp(playerPos.x, selectedTilePosition.x, prevPlayerStartTime),
            Mathf.Lerp(playerPos.y, selectedTilePosition.y, prevPlayerStartTime),
            Mathf.Lerp(playerPos.z, selectedTilePosition.z, prevPlayerStartTime));
        }
        

    }

    public Vector3 Id2Position(Vector3 id) {
        Vector3 result = id - tileMgr.GetIdLength()/2f;

        return result;
    }

    public Vector3 Id2Position(int x, int y, int z) {
        Vector3 result = new Vector3(x, y, z);
        result -= tileMgr.GetIdLength()/2f;

        return result;
    }

    public void SetCannotAccessSign(Vector3 tmpTilePosition) {

        var obj = Instantiate(CannotAccessObject);

        if (!isPositionXFunction()) {
            float tmp_x = tmpTilePosition.x + (!isFrontFunction() ? 0.6f : -0.6f);
            obj.transform.position = new Vector3(tmp_x, tmpTilePosition.y, tmpTilePosition.z);
        }
        else {
            float tmp_z = tmpTilePosition.z + (!isFrontFunction() ? 0.6f : -0.6f);
            obj.transform.position = new Vector3(tmpTilePosition.x, tmpTilePosition.y, tmp_z);
        }
    }
}
