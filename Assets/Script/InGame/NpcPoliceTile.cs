﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcPoliceTile : AttackTile {

    public Animator animator;

    private APlayer player;
    private GameSystemMgr gameSystemMgr;
    private CameraMgr cameraMgr;
    private Collider collider;

    private bool isMove;

    // Use this for initialization
    void Start () {
        player = GameObject.Find("player").GetComponent<APlayer>();
        gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
        cameraMgr = GameObject.Find("CameraManager").GetComponent<CameraMgr>();
        collider = GetComponent<SphereCollider>();
        _bIsCanIgnoreBlock = true;
        isMove = false;
    }
	
	// Update is called once per frame
	void Update () {

	    if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)) {
	        collider.enabled = false;
	    }
	    else {
	        collider.enabled = true;
	    }

	    if (!isMove && !cameraMgr.isRelax) {
	        isMove = true;
            animator.SetBool("isRun", true);
	    }else if (isMove && cameraMgr.isRelax) {
	        isMove = false;
            animator.SetBool("isRun", false);
	    }

	    if (gameSystemMgr.isFailed && !animator.GetBool("isArrest")) {
	        collider.enabled = true;
            animator.SetBool("isArrest", true);
	    }

        if (!Input.GetMouseButtonDown(0) && !Input.GetMouseButton(0))
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera.main.transform.rotation, 0.1f);

    }

    public override void SetLocalX(float x) {
        base.SetLocalX(x);
        //transform.position = new Vector3(x, transform.position.y, transform.position.z);
        if (!gameSystemMgr.isFailed && player.positionId.y + 1 == id.y && player.positionId.z == id.z) {
            if (!isMetPlayer()) return;
            gameSystemMgr.isFailed = true;
            transform.localPosition = player.transform.localPosition + new Vector3(0, 0.6f, 0);
        }
    }

    public override void SetLocalZ(float z) {
        base.SetLocalZ(z);
        //transform.position = new Vector3(transform.position.x, transform.position.y, z);
        if (!gameSystemMgr.isFailed && player.positionId.y + 1 == id.y && player.positionId.x == id.x) {
            if (!isMetPlayer()) return;
            gameSystemMgr.isFailed = true;
            transform.localPosition = player.transform.localPosition + new Vector3(0, 0.6f, 0);
        }
    }

    public new void SetRigid(bool activated) {
        base.SetRigid(activated);
    }

    public override void Init() {
        base.Init();

    }

    private bool isMetPlayer() {
        Ray ray = new Ray();
        bool isPositionX = gameSystemMgr.isPositionXFunction();

        if (!isPositionX) {
            ray.origin = new Vector3(-9, transform.position.y, transform.position.z);
            ray.direction = new Vector3(1, 0, 0);
        }
        else {
            ray.origin = new Vector3(transform.position.x, transform.position.y, -9);
            ray.direction = new Vector3(0, 0, 1);
        }

        RaycastHit[] raycasts = Physics.RaycastAll(ray.origin, ray.direction);

        float _Npc = !isPositionX ? transform.position.x : transform.position.z;
        Debug.Log("Npc : " + _Npc);

        float _Player = -9999;
        List<float> _Tile = new List<float>();

        foreach (RaycastHit t in raycasts) {
            GameObject _gameObject = t.collider.gameObject;
            float tmp_pos = !isPositionX ? _gameObject.transform.position.x : _gameObject.transform.position.z;


            if (_gameObject.CompareTag("Tile")) {

                TileObject tile = _gameObject.GetComponent<TileObject>();
                if (tile.isDestroyTile) continue;

                if (tile.id.y == id.y) {
                    Debug.Log("Tile : " + tmp_pos);
                    _Tile.Add(tmp_pos);
                }
                continue;
            }
            if (_gameObject.CompareTag("Player")) {
                    Debug.Log("Player : " + tmp_pos);
                _Player = tmp_pos;
            }
        }

        if (_Npc == -9999 || _Player == -9999) {
            return false;
        }

        for (int i = 0; i < _Tile.Count; i++) {
            float tmp = _Tile[i];

            if (_Npc > tmp && _Player < tmp ||
                _Player > tmp && _Npc < tmp) {
                return false;
            }
        }

        return true;
    }
}
