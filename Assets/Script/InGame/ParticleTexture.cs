﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTexture : MonoBehaviour {

    public int id;
    private Material material;

	// Use this for initialization
	void Start () {
		DataMgr dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        int stageId = ResourceManager.SetStageId(dataMgr.dataClass.CURRENT_STAGE_ID + 1);
        material = Resources.Load<Material>("Particle/episode" + (dataMgr.dataClass.CURRENT_EPISODE_ID + 1) + "/st" + stageId + "_" + id);

	    if (material != null) {
	        GetComponent<ParticleSystemRenderer>().material = material;
	        if (stageId == 1 && dataMgr.dataClass.CURRENT_EPISODE_ID == 1) {
	            GetComponent<ParticleSystemRenderer>().maxParticleSize = 0.04f;

	        }
	    }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
