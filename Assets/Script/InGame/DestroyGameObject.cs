﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyGameObject : MonoBehaviour {

    public GameObject _GameObject;

    public float Delay = 1f;
    public bool _Start = true;

    private float deltaTime = 0;

	// Use this for initialization
	void Start () {
	    if (_GameObject == null) {
	        _GameObject = gameObject;
	    }

        Reset();
	}
	
	// Update is called once per frame
	void Update () {
	    if (_Start) {
	        deltaTime += Time.deltaTime;

	        if (deltaTime >= Delay) {
	            _Start = false;
	            Destroy(_GameObject);
	        }
	    }
	}

    public void Reset() {
        deltaTime = 0;
    }

}
