﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APlayer : AActor {

    public bool isBlocked;
    public bool isPlayerMoving;
    
    private float AnimationDeltaTime;
    public GameObject PlayerMesh;

    private GameObject SpeechBubble;
    public float SpeechBubbleAnimationTime;

    private AudioSource audioSource;
    public AudioClip RunAudioClip;
    public AudioClip JumpAudioClip;
    public AudioClip LendAudioClip;
    public AudioClip ArrestAudioClip;
    public AudioClip PortalInAudioClip;
    public AudioClip PortalOutAudioClip;
    public AudioClip SpeechAudioClip;

    private bool isSoundActivated;
    private float RunSoundSupportTime;

    private bool isInPortal;
    private Vector3 targetScale;
    private Vector3 speechBubblePos;
    private float PortalTime;

    private GameSystemMgr gameSystemMgr;
    private TileMgr tileMgr;
    private CameraMgr cameraMgr;
    private DataMgr dataMgr;

    private TweenScale tweenScale;
    private bool isSpeechActived;
    private bool isUpdatingGame;
    private bool isSpeechAnimation;
    private string SpeechText;
    private UILabel SpeechLabel;
    private float SpeechBubbleDelayTime;

    // Use this for initialization
    void Start () {
	    isBlocked = false;
	    isInPortal = false;
        targetScale = Vector3.one;
	    gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
        cameraMgr = GameObject.Find("CameraManager").GetComponent<CameraMgr>();
	    tileMgr = GameObject.Find("TileManager").GetComponent<TileMgr>();
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        SpeechBubble = GameObject.Find("SpeechBubble");
        tweenScale = SpeechBubble.GetComponent<TweenScale>();
        SpeechLabel = GameObject.Find("speech_str").GetComponent<UILabel>();
        audioSource = GetComponent<AudioSource>();
        speechBubblePos = SpeechBubble.transform.localPosition;
        isSpeechActived = false;
        isSpeechAnimation = false;
        isUpdatingGame = false;
        isPlayerMoving = false;
        SpeechText = "";
        SpeechBubbleDelayTime = 0;
        AnimationDeltaTime = 0;
        RunSoundSupportTime = 0;

        ClosetList closetList = dataMgr.GetComponent<ClosetList>();
        


        dataMgr.playerTexture =
            Resources.Load<Texture2D>("pcskin/" + closetList.ClosetCategoryPath[dataMgr.dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX] + "/map/" +
                                      dataMgr.dataClass.CURRENT_PLAYER_SKIN);
        dataMgr.playerMesh =
            Resources.Load<Mesh>("pcskin/" + closetList.ClosetCategoryPath[dataMgr.dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX] + "/fbx/" +
                                 dataMgr.playerTexture.name);

        PlayerMesh.GetComponent<Renderer>().material.mainTexture = dataMgr.playerTexture;
        PlayerMesh.GetComponent<SkinnedMeshRenderer>().sharedMesh = dataMgr.playerMesh;

        audioSource.mute = dataMgr.dataClass.CURRENT_MUTE;
    }

    public override void Init() {
        base.Init();
        
        GameObject tmp = tileMgr.GetTile(positionId);
        if (tmp == null) return;

        TileObject temp_tile = tmp.GetComponent<TileObject>();
        string str = temp_tile.GetSpeechBubble();

		if (temp_tile._type == "PortalTile") {
            PortalTile tmp_porTile = temp_tile as PortalTile;
            tmp_porTile.SetPlayerSpawned();
        }

        if (!string.IsNullOrEmpty(str)) {

            tweenScale.enabled = true;

            tweenScale.@from = Vector3.zero;
            tweenScale.to = new Vector3(0.25f, 0.25f, 1);

            tweenScale.Reset();
            isSpeechActived = true;
            isSpeechAnimation = true;
            SpeechLabel.text = "";
            SpeechText = str;
            SpeechBubbleDelayTime = 0;
        }
    }

    // Update is called once per frame
	void Update () {

        PortalTime += Time.deltaTime;
	    AnimationDeltaTime += Time.deltaTime;

	    if (AnimationDeltaTime > 1) {
	        AnimationDeltaTime = AnimationDeltaTime - 1;
	    }

        if (transform.localScale != targetScale) {

	        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, PortalTime);
	    }

	    if (isSpeechActived && isSpeechAnimation) {
            SpeechBubbleDelayTime += Time.deltaTime;
            int index = SpeechLabel.text.Length;

	        if (index < SpeechText.Length && SpeechBubbleDelayTime > (float)SpeechBubbleAnimationTime * index) {
	            SpeechLabel.text += SpeechText[index];
                audioSource.clip = SpeechAudioClip;
                audioSource.Play(0);
            }
            else if (index >= SpeechText.Length) {
	            SpeechBubbleDelayTime = 0;
	            isSpeechAnimation = false;

	        }
	    }

	    if (gameSystemMgr.isPlayerMovingUp ||
            (isPlayerMoving && !isUpdatingGame) ||
            (!cameraMgr.isRelax && !isUpdatingGame) ||
            (gameSystemMgr.isPause && !isUpdatingGame)) {
	        isUpdatingGame = true;
            SpeechBubble.transform.localPosition = new Vector3(speechBubblePos.x, speechBubblePos.y, 0);

            SetSpeechBubble();
	    }else if (isUpdatingGame) {
	        if (!isPlayerMoving && cameraMgr.isRelax && !gameSystemMgr.isPause) {
	            isUpdatingGame = false;
                SpeechBubble.transform.localPosition = speechBubblePos;

                SetSpeechBubble();
            }
	    }

	    if (animator.GetBool("isRun")) {
	        if (isSoundActivated) {
	            audioSource.clip = RunAudioClip;
                audioSource.Play(0);
	            isSoundActivated = false;
	        }

	        if (AnimationDeltaTime > 0.08f + 0.36f * RunSoundSupportTime && !isSoundActivated) {
	            isSoundActivated = true;
	            RunSoundSupportTime++;
	            return;
	        }
	    }else if (RunSoundSupportTime > 0) {
            RunSoundSupportTime = 0;
	    }

	    if (!isSoundActivated) return;

	    isSoundActivated = false;

	    if (animator.GetBool("isJump")) {
            audioSource.clip = JumpAudioClip;
            audioSource.Play(0);
        }else if (animator.GetBool("isLand")) {
            audioSource.clip = LendAudioClip;
            audioSource.Play(0);
        }
        else {
	        if (animator.GetBool("isDisappear")) {
                audioSource.clip = PortalInAudioClip;
            }
	        else {
                audioSource.clip = PortalOutAudioClip;
            }
            
            audioSource.Play(0);
        }


    }

    void LateUpdate() {
        Quaternion q = Camera.main.transform.rotation;

        if (!cameraMgr.isRelax && Input.GetMouseButton(0)) return;
        
        transform.rotation = Quaternion.Lerp(transform.rotation, Camera.main.transform.rotation, 0.1f);
    }

    public void SetSpeechBubble(string speech = null) {
        TileObject tmp = tileMgr.GetTile(positionId).GetComponent<TileObject>();
        string str = tmp.GetSpeechBubble();

        if (speech != null) {
            str = speech;
            isSpeechActived = true;
            isSpeechAnimation = true;
        }

        if (!string.IsNullOrEmpty(str) && !isUpdatingGame) {
            tweenScale.enabled = true;

            tweenScale.@from = Vector3.zero;
            tweenScale.to = new Vector3(0.25f, 0.25f, 1);

            tweenScale.Reset();
            isSpeechActived = true;
            isSpeechAnimation = true;
            SpeechText = str;
            SpeechLabel.text = "";
            SpeechBubbleDelayTime = 0;
        }
        else if (isSpeechActived) {
            isSpeechActived = false;
            isSpeechAnimation = false;
            tweenScale.enabled = true;

            tweenScale.@from = new Vector3(0.25f, 0.25f, 1);
            tweenScale.to = Vector3.zero;

            tweenScale.Reset();
            SpeechText = "";
            SpeechBubbleDelayTime = 0;

        }
    }

    public override void Die() {
        
    }

    public override void ResetAnimationDeltaTime() {
        base.ResetAnimationDeltaTime();

        AnimationDeltaTime = 0;
        isSoundActivated = true;
    }

    public void OnTakePortal() {

        isInPortal = !isInPortal;

       
        animator.SetBool("isDisappear", isInPortal);
        
        PortalTime = 0;

        if (isInPortal) {
            ResetAnimationDeltaTime();
            targetScale = Vector3.zero;
            PortalTime = -0.5f;
        }
        else {
            ResetAnimationDeltaTime();
            targetScale = Vector3.one;
        }

    }
   

}
