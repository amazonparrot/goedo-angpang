﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcDogTile : AActor {

    public bool isEnabled;
    public bool isLeftMove;

    private GameSystemMgr gameSystemMgr;
    private StageMgr stageMgr;
    private TileMgr tileMgr;
    private CameraMgr cameraMgr;

    private Vector3 prevPosition;
    private float prevPlayerStartTime;

    private float MovingTime;
    private Vector3 selectedTilePosition;
    private Vector3 lerpedPosition;

    private bool isOrderingAgain = false;

    private Rigidbody m_rigidbody;
    private SphereCollider collider;


    // Use this for initialization
    void Start () {
	    gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
        stageMgr = GameObject.Find("StageManager").GetComponent<StageMgr>();
        tileMgr = gameSystemMgr.TileManager.GetComponent<TileMgr>();
        cameraMgr = tileMgr.CameraManager.GetComponent<CameraMgr>();
        m_rigidbody = GetComponent<Rigidbody>();
        collider = GetComponent<SphereCollider>();
        MovingTime = 1;

    }
	
	// Update is called once per frame
	void Update () {

	    if (!isEnabled) return;
	    if (gameSystemMgr.isFailed || gameSystemMgr.isCleared) {
	        if (gameSystemMgr.isFailed) {
                m_rigidbody.useGravity = true;
                m_rigidbody.isKinematic = false;
	            collider.isTrigger = false;
	            collider.radius = 1.00f;
                collider.center = new Vector3(0.37f, -0.96f, 0.17f);
                animator.SetBool("isArrest", true);
            }
	        isEnabled = false;
	        return;
	    }

	    if (gameSystemMgr.isPause || gameSystemMgr.isPortalArrived || stageMgr.isPortalUsing) {
	        return;
	    }

        isEnabled = transform.localPosition != new Vector3(-100, -100, -100);


	    if (cameraMgr.isRelax) {
	        if (isMetPlayer()) {
	            gameSystemMgr.isFailed = true;
	            return;
	        }
	    }

        if (gameSystemMgr.isNpcOrdered) {
	        OnTileTouched();
            if (isOrderingAgain) {
                isOrderingAgain = false;
                OnTileTouched();
            }
	    }

	    if (gameSystemMgr.isPlayerMoving) {
            OnNpcMove();
        }

	}

    void LateUpdate() {

        if (!isEnabled) return;

        if (prevPlayerStartTime >= MovingTime) {
            prevPlayerStartTime = 0;
            transform.position = position = selectedTilePosition;
            prevPosition = transform.position;
            animator.SetBool("isRun", false);
        }

        if (!Input.GetMouseButtonDown(0) && !Input.GetMouseButton(0))
            transform.rotation = Quaternion.Lerp(transform.rotation, Camera.main.transform.rotation, 0.1f);
    }

    void OnTriggerEnter(Collider col) {
        if (col.tag == "Player") {
            gameSystemMgr.isFailed = true;
        }
    }

    public override void Init() {
        base.Init();

        isEnabled = true;

    }

    public override void ResetAnimationDeltaTime() {
        base.ResetAnimationDeltaTime();
    }

    public override void Die() {
        base.Die();
    }

    public void OnTileTouched() {

        Vector3 _id = positionId + Vector3.down;
        bool isPositionX = gameSystemMgr.isPositionXFunction();
        bool isFront = gameSystemMgr.isFrontFunction();

        if (isLeftMove) {
            if (isPositionX) {
                _id += new Vector3(-1, 0, 0);
            }
            else {
                _id += new Vector3(0, 0, -1);
            }
        }
        else {
            if (isPositionX) {
                _id += new Vector3(1, 0, 0);
            }
            else {
                _id += new Vector3(0, 0, 1);
            }
        }
        GameObject _gameObject = tileMgr.GetSurfaceTile(isPositionX, isFront, _id);

        if (_gameObject == null) {
            isLeftMove = !isLeftMove;
            prevPlayerStartTime = 0;
            isOrderingAgain = true;
            return;
        }

        if (_gameObject.GetComponent<TileObject>()._type == "VanishTile") {
            VanishTile tile_tmp = _gameObject.GetComponent<VanishTile>();

            if (tile_tmp.isDestroyTile) {
                m_rigidbody.useGravity = true;
                m_rigidbody.isKinematic = false;
                collider.enabled = false;
                isEnabled = false;
                return;
            }
        }

        Vector3 _SurfaceId = _gameObject.GetComponent<TileObject>().id;

        if (_SurfaceId == -Vector3.one) return;
        if (positionId == _SurfaceId) return;

        if (!gameSystemMgr.OnJudgeActorMovable(_SurfaceId, this)) {
            isLeftMove = !isLeftMove;
            prevPlayerStartTime = 0;
            isOrderingAgain = true;
            return;
        }
        selectedTilePosition = _gameObject.transform.position + Vector3.up;

        prevPosition = transform.position;
        prevPlayerStartTime = 0;

   

        if (gameSystemMgr.isPositionXFunction()) {
            bool tmp = transform.position.x - selectedTilePosition.x > 0;

            if (!isFront) {
                lerpedPosition = new Vector3((tmp
                    ? 1
                    : -1), 0, tmp ? 0 : 1);
            }
            else {
                lerpedPosition = new Vector3((tmp
                                 ? 1
                                 : -1), 0, tmp ? -2 : 0);
            }
        }
        else {
            bool tmp = transform.position.z - selectedTilePosition.z > 0;

            if (!isFront) {
                lerpedPosition = new Vector3(tmp ? 2 : 0, 0, (tmp
                                 ? 1
                                 : -1));
            }
            else {
                lerpedPosition = new Vector3(tmp ? 0 : -2, 0, (tmp
                                 ? 1
                                 : -1));
            }

        }

        positionId = _SurfaceId + Vector3.up;
        animator.SetBool("isRun", true);
        isOrderingAgain = false;

        if (_gameObject.GetComponent<TileObject>()._type == "VanishTile") {
            VanishTile tile_tmp = _gameObject.GetComponent<VanishTile>();

            tile_tmp.SetActorMet();

        }

    }

    private void OnNpcMove() {

        if (isOrderingAgain) return;

        Vector3 playerPos = prevPosition;
        prevPlayerStartTime += Time.deltaTime * 1.5f;

        bool run = animator.GetBool("isRun");
        bool isPositionX = gameSystemMgr.isPositionXFunction();


        Vector3 dir = transform.position - selectedTilePosition;
        Quaternion lookRot = Quaternion.LookRotation(lerpedPosition);
        lookRot.x = 0; lookRot.z = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, 0.1f);


        if (run) {
            transform.position = new Vector3(Mathf.Lerp(playerPos.x, selectedTilePosition.x, prevPlayerStartTime),
            Mathf.Lerp(playerPos.y, selectedTilePosition.y, prevPlayerStartTime),
            Mathf.Lerp(playerPos.z, selectedTilePosition.z, prevPlayerStartTime));
        }


    }

    private bool isMetPlayer() {
        Ray ray = new Ray();
        bool isPositionX = gameSystemMgr.isPositionXFunction();

        if (!isPositionX) {
            ray.origin = new Vector3(-9, transform.position.y, transform.position.z);
            ray.direction = new Vector3(1, 0, 0);
        }
        else {
            ray.origin = new Vector3(transform.position.x, transform.position.y, -9);
            ray.direction = new Vector3(0, 0, 1);
        }

        RaycastHit[] raycasts = Physics.RaycastAll(ray.origin, ray.direction);
        
        float _Npc = -9999;
        float _Player = -9999;
        List<float> _Tile = new List<float>();

        foreach (RaycastHit t in raycasts) {
            GameObject _gameObject = t.collider.gameObject;
            float tmp_pos = !isPositionX ? _gameObject.transform.position.x : _gameObject.transform.position.z;


            if (_gameObject.CompareTag("Tile")) {

                TileObject tile = _gameObject.GetComponent<TileObject>();
                if (tile.isDestroyTile) continue;

                if (tile.id.y == positionId.y) {
                    _Tile.Add(tmp_pos);
                }
                continue;
            }
            if (_gameObject == gameObject) {
                _Npc = tmp_pos;
                continue;
            }
            if (_gameObject.CompareTag("Player")) {
                _Player = tmp_pos;
            }
        }

        if (_Npc == -9999 || _Player == -9999) {
            return false;
        }

        for (int i = 0; i < _Tile.Count; i++) {
            float tmp = _Tile[i];

            if (_Npc > tmp && _Player < tmp ||
                _Player > tmp && _Npc < tmp) {
                return false;
            }
        }

        transform.position = GameObject.Find("player").transform.position + new Vector3(0, 0.6f, 0);

        return true;
    }
}
