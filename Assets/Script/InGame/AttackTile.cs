﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTile : TileObject {

    public bool isTileEnable = true;

	// Use this for initialization
	void Start () {
	    isTileEnable = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void Init() {
        base.Init();
    }

    public bool isBehindEmpty() {
        Ray ray = new Ray();

        if (!GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>().isPositionXFunction()) {
            ray.origin = new Vector3(-9, transform.position.y, transform.position.z);
            ray.direction = new Vector3(1, 0, 0);
        }
        else {
            ray.origin = new Vector3(transform.position.x, transform.position.y, -9);
            ray.direction = new Vector3(0, 0, 1);
        }

        RaycastHit[] raycasts = Physics.RaycastAll(ray.origin, ray.direction);
        foreach (RaycastHit t in raycasts) {
            GameObject _gameObject = t.collider.gameObject;

            if (!_gameObject.CompareTag("Tile")) continue;
            if (_gameObject.GetComponent<TileObject>().id + Vector3.up == id) continue;
            
            return false;
        }
        return true;
    }
}
