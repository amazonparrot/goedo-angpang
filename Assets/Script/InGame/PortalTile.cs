﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTile : TileObject {
    
    private APlayer player;
    private GameSystemMgr gameSystemMgr;
    private StageMgr stageMgr;
    private CameraMgr cameraMgr;

    public Vector3 targetTileId;
    public int targetStage;
    public Color portalColor;

    private bool isPortalActived;
    private ParticleSystem particleSettings_energy;
    private GameObject particle_energy;
    private ParticleSystem particleSettings_flash;
    private bool isPlayerStartPosition;

    void Awake() {
        targetTileId = -Vector3.one;
        targetStage = -2;
        isPortalActived = false;

        for (int i = 0; i < transform.childCount; i++) {
            GameObject tmp = transform.GetChild(i).gameObject;

            if (tmp.transform.name == "Ingredient-CandleType-Flash2") {
                particle_energy = tmp;
                particleSettings_energy = tmp.GetComponent<ParticleSystem>();
                continue;
            }

            if (tmp.transform.name == "angpang_partal") {
                particleSettings_flash = tmp.GetComponent<ParticleSystem>();
            }
        }
    }

    // Use this for initialization
    void Start () {
        _type = "PortalTile";
        player = GameObject.Find("player").GetComponent<APlayer>();
        gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
        stageMgr = GameObject.Find("StageManager").GetComponent<StageMgr>();
        cameraMgr = GameObject.Find("CameraManager").GetComponent<CameraMgr>();


    }

    public override void Init() {
        base.Init();
        isPortalActived = false;
        isPlayerStartPosition = false;
    }

    // Update is called once per frame
	void Update () {

	    if (!isPortalActived && gameSystemMgr.isPlayerMoving) {
	        isPortalActived = true;
	        isPlayerStartPosition = false;
	    }

	    if (!cameraMgr.isRelax && Input.GetMouseButtonUp(0)) {
	        var energy = particleSettings_energy.main;
	        var flash = particleSettings_flash.main;
            energy.startColor = new Color(portalColor.r / 4, portalColor.g / 4, portalColor.b / 4, 0.5f);
            flash.startColor = new Color(portalColor.r / 4, portalColor.g / 4, portalColor.b / 4, 0);
        }
	}

    public override void SetLocalX(float x) {
        base.SetLocalX(x);
        var energy = particleSettings_energy.main;
        var flash = particleSettings_flash.main;
        energy.startColor = portalColor;
        flash.startColor = portalColor;

        if (!isPortalActived) return;
        //transform.position = new Vector3(x, transform.position.y, transform.position.z);
        if (player.positionId.y == id.y && player.positionId.z == id.z) {
            if (targetStage == -2) return;
            gameSystemMgr.isPortalArrived = true;
            gameSystemMgr.isCleared = false;
            gameSystemMgr.m_nextStageId = targetStage;
            stageMgr.SetPlayerId(targetTileId);
            
        }
    }

    public override void SetLocalZ(float z) {
        base.SetLocalZ(z);
        var energy = particleSettings_energy.main;
        var flash = particleSettings_flash.main;
        energy.startColor = portalColor;
        flash.startColor = portalColor;

        if (!isPortalActived) return;
        //transform.position = new Vector3(transform.position.x, transform.position.y, z);
        if (player.positionId.y == id.y && player.positionId.x == id.x) {
            if (targetStage == -2) return;
            gameSystemMgr.isPortalArrived = true;
            gameSystemMgr.isCleared = false;
            gameSystemMgr.m_nextStageId = targetStage;
            stageMgr.SetPlayerId(targetTileId);
        }
    }

    public void SetTargetStage(int stage) {
        targetStage = stage;
    }

    public void SetTargetTileId(Vector3 id, Color color) {
        targetTileId = id;
    }

    public void SetPortalColor(Color color) {
        portalColor = new Color(color.r, color.g, color.b, color.a);
        var energy = particleSettings_energy.main;
        var flash = particleSettings_flash.main;
        energy.startColor = portalColor;
        flash.startColor = portalColor;

        Debug.Log(name + " Color : " + color.r + ", " + color.g + ", " + color.b);
    }

    public void SetPlayerSpawned() {
        isPlayerStartPosition = true;
    }
}
