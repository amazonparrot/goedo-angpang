﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanishTile : TileObject {

    public int VanishCount;
    public int VisibleCurrentCount;
    

    public int CurrentCount;
    private APlayer player;
    private GameSystemMgr gameSystemMgr;
    private StageMgr stageMgr;

    private Material mat;
    private Color targetColor;
    private bool isPlayerMet;

    // Use this for initialization
    void Start () {
	    _type = "VanishTile";
        player = GameObject.Find("player").GetComponent<APlayer>();
        gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
        stageMgr = GameObject.Find("StageManager").GetComponent<StageMgr>();

        for (var i = 0; i < transform.childCount; i++) {
            var tmp = transform.GetChild(i).gameObject;

            if (tmp.transform.name == "VanishTile_Mesh") {
                mat = tmp.GetComponent<Renderer>().material;
                targetColor = new Color(1, 1, 1, 1);
            }
        }

        VisibleCurrentCount = CurrentCount = VanishCount;
    }

    // Update is called once per frame
    void Update () {
        if (gameSystemMgr.isPlayerMovingUp && !isPlayerMet && !stageMgr.isPortalUsing &&
            !IsBehindObject(transform.position + Vector3.up, "Actor")) {
            if (CurrentCount <= 0) {
                isDestroyTile = true;
                VisibleCurrentCount = 0;
                CurrentCount = -1;
                if (GetOtherTileObject() == null) {
                    GetComponent<BoxCollider>().enabled = false;
                }
            }


            VisibleCurrentCount = CurrentCount < 0 ? 0 : CurrentCount;
            targetColor.a = (float)VisibleCurrentCount / VanishCount;
        }


        if (gameSystemMgr.isPlayerMovingUp) {
            isPlayerMet = false;
        }

        
        mat.color = Color.Lerp(mat.color, targetColor, 0.3f);

    }

    public override void Init() {
        base.Init();
        
        
    }

    public override void SetLocalX(float x) {
        base.SetLocalX(x);
        if (stageMgr.isPortalUsing) return;

        if (player.positionId.y == id.y && player.positionId.z == id.z) {
            if (CurrentCount > 0 && !isPlayerMet) {
                CurrentCount--;
                isPlayerMet = true;
                if (CurrentCount == 0) {
                    targetColor.g = 0.7f;
                    targetColor.b = 0.7f;
                    isNotAccess = true;
                }
            }

        }

    }

    public override void SetLocalZ(float z) {
        base.SetLocalZ(z);
        if (stageMgr.isPortalUsing) return;

        if (player.positionId.y == id.y && player.positionId.x == id.x) {
            if (CurrentCount > 0 && !isPlayerMet) {
                CurrentCount--;
                isPlayerMet = true;
                if (CurrentCount == 0) {
                    targetColor.g = 0.7f;
                    targetColor.b = 0.7f;
                    isNotAccess = true;
                }
            }
        }

    }

    public void SetActorMet() {
        if (stageMgr.isPortalUsing) return;
        if (CurrentCount > 0 && !isPlayerMet) {
            CurrentCount--;
            isPlayerMet = true;
            if (CurrentCount == 0) {
                targetColor.g = 0.7f;
                targetColor.b = 0.7f;
                isNotAccess = true;
            }
        }

    }
}
