﻿using System;
using UnityEngine;

public class TileObject : AGameObject {

    public string _type = "TileObject";
    public bool isDestroyTile;
    public bool isNotAccess;
    private bool _bIsRigidActivated;
    protected bool _bIsCanIgnoreBlock;
    protected string _speechBubbleStr = "";

    private Rigidbody m_rigidbody;

    private CameraMgr cameraMgr;
    private bool isResetCollider;
    private bool isLerpResetCollider;

    public Vector3 id;

    // Use this for initialization
    protected void Start() {
        _bIsRigidActivated = false;
        SetRigidBody(_bIsRigidActivated);
        SetGravity(false);
        _bIsCanIgnoreBlock = false;
        isDestroyTile = false;

        cameraMgr = GameObject.Find("CameraManager").GetComponent<CameraMgr>();
    }

    // Update is called once per frame
    protected void Update() {
        

        if (isResetCollider) {
            if (Input.GetMouseButton(0) && !cameraMgr.isRelax) {
                ResetCollider();
                isResetCollider = false;
                isLerpResetCollider = false;

            }

        }

        if (isLerpResetCollider) {
            if (Input.GetMouseButtonDown(0) && cameraMgr.isRelax) {
                if(GetBehindTile(transform.position + Vector3.up) == null)
                    ResetCollderClickable();
            }
        }

    }

    // 타일이 생성되고 포지션이 설정된 뒤 초기설정을 하는 함수입니다.
    public virtual void Init() {
        ResetCollider();
    }

    public void SetRigid(bool activated) {
        if (_bIsRigidActivated != activated) SetRigidBody(activated);

        _bIsRigidActivated = activated;
    }


    public virtual void SetLocalX(float x) {
        TileObject obj = GetBehindTile(transform.position + Vector3.up);
        if (obj == null) {
            ResetCollderClickable();
        }else if (obj._bIsCanIgnoreBlock) {
            ResetCollderClickable();
        }else if (obj._type == "VanishTile") {
            isLerpResetCollider = true;
        }
        //transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }

    public virtual void SetLocalZ(float z) {
        TileObject obj = GetBehindTile(transform.position + Vector3.up);
        if (obj == null) {
            ResetCollderClickable();
        }
        else if (obj._bIsCanIgnoreBlock) {
            ResetCollderClickable();
        }else if (obj._type == "VanishTile") {
            isLerpResetCollider = true;
        }
        //transform.position = new Vector3(transform.position.x, transform.position.y, z);
    }

    public virtual void OnPlayerMoved() {
        
    }

    public TileObject GetOtherTileObject() {
        return GetBehindTile(transform.position);
    }

    public void ResetCollider() {
        BoxCollider collider = GetComponent<BoxCollider>();
        if (collider == null) return;
        collider.size = Vector3.one;
        collider.center = Vector3.zero;
    }

    public void ResetCollderClickable() {
        BoxCollider collider = GetComponent<BoxCollider>();
        if (collider == null) return;
        collider.size = new Vector3(1, 1, 2);
        collider.center = Vector3.forward/2;

        isResetCollider = true;
    }

    public bool IsCanPlayerIgnoreBlock() {
        return _bIsCanIgnoreBlock;
    }

    public void SetGravity(bool activated) {
        if (m_rigidbody == null)
            return;
        m_rigidbody.useGravity = activated;
        m_rigidbody.isKinematic = !activated;
    }

    public string GetSpeechBubble() {
        return _speechBubbleStr;
    }

    public void SetSpeechBubble(string str) {
        _speechBubbleStr = str;
    }

    protected void SetRigidBody(bool activated) {
        if (activated && (m_rigidbody == null))
            m_rigidbody = gameObject.AddComponent<Rigidbody>();

        else if (!activated && (m_rigidbody != null)) {
            Destroy(m_rigidbody);
            m_rigidbody = null;
        }
    }

    private TileObject GetBehindTile(Vector3 rayPosition) {
        Ray ray = new Ray();

        if (!GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>().isPositionXFunction()) {
            ray.origin = new Vector3(-9, rayPosition.y, rayPosition.z);
            ray.direction = new Vector3(1, 0, 0);
        }
        else {
            ray.origin = new Vector3(rayPosition.x, rayPosition.y, -9);
            ray.direction = new Vector3(0, 0, 1);
        }

        RaycastHit[] raycasts = Physics.RaycastAll(ray.origin, ray.direction);
        foreach (RaycastHit t in raycasts) {
            GameObject _gameObject = t.collider.gameObject;

            if (!_gameObject.CompareTag("Tile")) continue;

            TileObject tmp_tile = _gameObject.GetComponent<TileObject>();
            if (tmp_tile.isDestroyTile)   continue;

            if(Math.Abs(tmp_tile.transform.position.y - rayPosition.y) > 0)   continue;


            return _gameObject.GetComponent<TileObject>();

        }

        return null;
    }

    public static bool IsBehindObject(Vector3 rayPosition, string _tag) {
        Ray ray = new Ray();

        if (!GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>().isPositionXFunction()) {
            ray.origin = new Vector3(-9, rayPosition.y, rayPosition.z);
            ray.direction = new Vector3(1, 0, 0);
        }
        else {
            ray.origin = new Vector3(rayPosition.x, rayPosition.y, -9);
            ray.direction = new Vector3(0, 0, 1);
        }

        RaycastHit[] raycasts = Physics.RaycastAll(ray.origin, ray.direction);
        foreach (RaycastHit t in raycasts) {
            GameObject _gameObject = t.collider.gameObject;

            if (!_gameObject.CompareTag(_tag)) continue;



            return true;

        }

        return false;
    }

}