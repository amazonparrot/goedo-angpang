﻿using UnityEngine;

public class ClearStarsUI : MonoBehaviour {
    private float accrueTime;

    private AudioSource audioSource;

    private GameSystemMgr gameSystemMgr;

    private DataMgr dataMgr;

    private ClearStarUI[] stars;

    // Use this for initialization
    private void Start() {
        gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        audioSource = GetComponent<AudioSource>();
        audioSource.mute = dataMgr.dataClass.CURRENT_MUTE;
        stars = new ClearStarUI[3];
        accrueTime = 0;

        for (var i = 0; i < transform.childCount; i++) {
            var tmp = transform.GetChild(i).gameObject;

            if (tmp.transform.name == "clear_star_1") {
                stars[0] = tmp.GetComponent<ClearStarUI>();
                continue;
            }

            if (tmp.transform.name == "clear_star_2") {
                stars[1] = tmp.GetComponent<ClearStarUI>();
                continue;
            }

            if (tmp.transform.name == "clear_star_3") {
                stars[2] = tmp.GetComponent<ClearStarUI>();
            }
        }
    }

    // Update is called once per frame
    private void Update() {
        if (!stars[0].isEnable && (accrueTime >= 1)) {
            stars[0].SetEnable(true);
        }

        if (!stars[1].isEnable && (accrueTime >= 1.3f))
            if (gameSystemMgr.m_playerScore > 1) {
                audioSource.Play();
                stars[1].SetEnable(true);
            }

        if (!stars[2].isEnable && (accrueTime >= 2f))
            if (gameSystemMgr.m_playerScore > 2) {
                audioSource.Play();
                stars[2].SetEnable(true);
            }

        accrueTime += Time.deltaTime;
    }
}