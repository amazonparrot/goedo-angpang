﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailPanelUI : MonoBehaviour {

    public GameObject particle;


    private GameSystemMgr gameSystemMgr;
    private ParticleSystem particleSystem;
    private TweenPosition _tweenPosition;
    private UIPanel panel;

    private float accrueTime;
    private bool isAnimationEnded;

	// Use this for initialization
	void Start () {
	    panel = GetComponent<UIPanel>();
	    particleSystem = particle.GetComponent<ParticleSystem>();
	    _tweenPosition = GetComponent<TweenPosition>();
	    gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();

	    accrueTime = 0;
	    isAnimationEnded = false;

        transform.localScale = Vector3.one;
	}
	
	// Update is called once per frame
	void Update () {
	    if (gameSystemMgr.isFailed && !isAnimationEnded) {
	        accrueTime += Time.deltaTime;

	        if (accrueTime > 1f) {

	            if (panel.alpha <= 0) {     //진영이 바보
	                _tweenPosition.enabled = true;
	            }

	            if (panel.alpha < 1) {
	                panel.alpha += 0.05f;
	            }else if (panel.alpha >= 1) {
	                isAnimationEnded = true;
                    particle.SetActive(true);

	            }
	        }

	    }
	}
}
