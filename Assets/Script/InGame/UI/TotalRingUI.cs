﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotalRingUI : MonoBehaviour {

    public GameObject spriteObject;
    public GameObject labelObject;

    private TweenAlpha spriteAlpha;
    private TweenAlpha labelAlpha;

    protected UILabel label;

    protected GameSystemMgr gameSystemMgr;
    private bool isInit;
    private bool isGoal;

    private int currentSwitchCount;

	// Use this for initialization
	protected void Start () {
	    spriteAlpha = spriteObject.GetComponent<TweenAlpha>();
	    labelAlpha = labelObject.GetComponent<TweenAlpha>();
	    label = labelObject.GetComponent<UILabel>();

	    gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
	    currentSwitchCount = 0;

	}

    // Update is called once per frame
    protected void Update () {



        if (gameSystemMgr.m_switchMaximum > 0) {
            label.text = gameSystemMgr.m_switchCount + "/" + gameSystemMgr.m_switchMaximum;

            if (currentSwitchCount != gameSystemMgr.m_switchCount) {
                TweenScale scale = labelObject.GetComponent<TweenScale>();
                scale.enabled = true;
                scale.Reset();
                currentSwitchCount = gameSystemMgr.m_switchCount;
            }


            if (!isInit) {
                isInit = true;

                spriteAlpha.enabled = true;
                labelAlpha.enabled = true;
            }

            if (gameSystemMgr.m_switchMaximum - gameSystemMgr.m_switchCount <= 0) {
                if (!isGoal) {
                    isGoal = true;

                    spriteObject.GetComponent<TweenColor>().enabled = true;

                }
            }

        }
    }
}
