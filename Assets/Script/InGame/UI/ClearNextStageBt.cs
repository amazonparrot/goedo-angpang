﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ClearNextStageBt : MonoBehaviour {
    private DataMgr dataMgr;

    // Use this for initialization
    private void Start() {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
    }

    // Update is called once per frame
    private void Update() {
    }

    private void OnClick() {
        if (dataMgr.dataClass.CURRENT_STAGE_ID + 1 <
            dataMgr.dataClass.EPISODE_DATA[dataMgr.dataClass.CURRENT_EPISODE_ID].STAGE_SIZE) {
            dataMgr.dataClass.CURRENT_STAGE_ID++;
            dataMgr.SaveData();

            StartCoroutine(DataMgr.SetScene(SceneManager.GetActiveScene().name));
        }
        else {
            StartCoroutine(DataMgr.SetScene("CartoonScene"));
        }
    }
}