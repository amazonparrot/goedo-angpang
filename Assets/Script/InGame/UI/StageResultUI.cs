﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

public class StageResultUI : MonoBehaviour {

    public AudioClip ClearAudioClip;
    public AudioClip FailAudioClip;

    public GameObject RemoveAdsPopUp;

    protected GameSystemMgr gameSystemMgr;
    protected AudioSource audioSource;
    protected DataMgr dataMgr;
    protected float accrueTime;
    private GameObject stage_result;

    protected GameObject clear_background;
    protected GameObject clear_layer2;
    protected GameObject clear_layer3;
    protected GameObject particle;

    private GameObject fail_panel;

    protected bool isPlayed;


    // Use this for initialization
    protected void Start () {
        transform.localPosition= new Vector3(0, 200, 0);
	    gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
        audioSource = GetComponent<AudioSource>();
        clear_background = GameObject.Find("clear_background");
        particle = GameObject.Find("clear_background_particle");
        fail_panel = GameObject.Find("fail_panel");
        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        particle.SetActive(false);
        //stage_result = GameObject.Find("stage_result");
        accrueTime = 0;
        isPlayed = false;

#if UNITY_ANDROID


        if (Advertisement.isSupported) {
            Advertisement.Initialize(dataMgr.AndroidGameId, true);
        }
#endif
    }
	
	// Update is called once per frame
	void Update () {
	    if (gameSystemMgr.isCleared || gameSystemMgr.isFailed) {

	        if (accrueTime == 0 && gameSystemMgr.isFailed) {
                //stage_result.GetComponent<UISprite>().spriteName = "result_txt_over";
                transform.localPosition = Vector3.zero;
	            if (!isPlayed) {

                    //광고
	                if (!dataMgr.dataClass2.CURRENT_ATZ && dataMgr.dataClass.CURRENT_EPISODE_ID != 0) {
	                    int r = Random.Range(0, 3);

	                    if (r == 0) {
	                        ShowRewardedVideo();
	                    }
	                }

                    audioSource.mute = dataMgr.dataClass.CURRENT_MUTE;
                    audioSource.clip = FailAudioClip;
                    audioSource.Play();
	                isPlayed = true;
	            }
	            
	        }
            else if (accrueTime >= 1 && gameSystemMgr.isCleared &&
                !clear_background.GetComponent<TweenScale>().enabled) {
                transform.localPosition = Vector3.zero;
	            clear_background.GetComponent<TweenScale>().enabled = true;
                if (!isPlayed) {
                    audioSource.mute = dataMgr.dataClass.CURRENT_MUTE;
                    audioSource.clip = ClearAudioClip;
                    audioSource.Play();
                    isPlayed = true;
                }
            }

	        if (gameSystemMgr.isCleared) {
	            if (clear_layer2 == null && accrueTime >= 1.5f) {
                    clear_layer2 = GameObject.Find("clear_panel_layer_2");
	                clear_layer2.GetComponent<TweenScale>().enabled = true;
                    clear_layer2.GetComponent<ClearStarsUI>().enabled = true;
                    particle.SetActive(true);
                }

	            if (clear_layer3 == null && accrueTime >= 4f) {
                    clear_layer3 = GameObject.Find("clear_panel_layer_3");
                    clear_layer3.GetComponent<TweenScale>().enabled = true;
                }
	        }

            accrueTime += Time.deltaTime;
        }
	}

    protected void ShowRewardedVideo() {
#if UNITY_ANDROID

        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show("video", options);
#endif
    }
#if UNITY_ANDROID

    void HandleShowResult(ShowResult result) {
        if (result == ShowResult.Finished) {

        }
        else if (result == ShowResult.Skipped) {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");

        }
        else if (result == ShowResult.Failed) {
            Debug.LogError("Video failed to show");
        }

        var tmp = Instantiate(RemoveAdsPopUp, GameObject.Find("Anchor").transform);
        tmp.transform.localPosition = new Vector3(0, 0, -400);
    }
#endif
}
