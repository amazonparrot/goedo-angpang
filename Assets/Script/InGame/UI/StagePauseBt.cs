﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagePauseBt : MonoBehaviour {

    public GameObject pauseUI;

    private StagePauseUI _stagePauseUi;

	// Use this for initialization
	void Start () {
	    _stagePauseUi = pauseUI.GetComponent<StagePauseUI>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Application.platform == RuntimePlatform.Android) {

            if (Input.GetKeyUp(KeyCode.Escape)) {

                OnClick();

            }

        }
    }

    void OnClick() {
        _stagePauseUi.isPause = !_stagePauseUi.isPause;
        _stagePauseUi.OnClickedEvent();
    }
}
