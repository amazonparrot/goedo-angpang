﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagePauseUI : MonoBehaviour {

    public bool isPause = false;

    private TweenScale tweenScale;
    private GameSystemMgr gameSystemMgr;
    private DataMgr dataMgr;
    private StageMgr stageMgr;
    private UILabel pop_pouse_stage_label;

    // Use this for initialization
    void Start () {
	    isPause = false;
	    tweenScale = GetComponent<TweenScale>();
	    gameSystemMgr = GameObject.Find("GameSystemManager").GetComponent<GameSystemMgr>();
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        stageMgr = GameObject.Find("StageManager").GetComponent<StageMgr>();
        transform.localScale = new Vector3(0.5f, 0, 0.5f);
        var pop_pouse_stage_label_obj = GameObject.Find("pop_pouse_stage_label");
        if (pop_pouse_stage_label_obj != null)
            pop_pouse_stage_label = pop_pouse_stage_label_obj.GetComponent<UILabel>();
    }
	
	// Update is called once per frame
	void Update () {

	}

    void LateUpdate() {
        gameSystemMgr.isPause = isPause;
    }

    public void OnClickedEvent() {
        if (gameSystemMgr.isFailed || gameSystemMgr.isPortalArrived || gameSystemMgr.isCleared) return;

        var pop_pouse_stage_label_obj = GameObject.Find("pop_pouse_stage_label");
        
        if (pop_pouse_stage_label_obj != null)
            pop_pouse_stage_label = pop_pouse_stage_label_obj.GetComponent<UILabel>(); tweenScale.Reset();
        tweenScale.enabled = false;
        pop_pouse_stage_label.text = "STAGE " + ((dataMgr.dataClass.CURRENT_EPISODE_ID * 15) + dataMgr.dataClass.CURRENT_STAGE_ID + 1);
        

        if (isPause && !tweenScale.enabled) {
            tweenScale.enabled = true;

            tweenScale.@from = new Vector3(0.5f, 0, 0.5f);
            tweenScale.to = Vector3.one;

            tweenScale.Reset();
        }
        if (!isPause && !tweenScale.enabled) {
            tweenScale.enabled = true;

            tweenScale.@from = Vector3.one;
            tweenScale.to = new Vector3(0.5f, 0, 0.5f);

            tweenScale.Reset();
        }
    }

    public void SetPause(bool pause) {
        if (gameSystemMgr.isFailed || gameSystemMgr.isPortalArrived || gameSystemMgr.isCleared) return;
        isPause = pause;
    }
}
