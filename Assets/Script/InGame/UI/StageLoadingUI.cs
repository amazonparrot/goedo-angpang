﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageLoadingUI : MonoBehaviour {

    private TileMgr tileMgr;

	// Use this for initialization
	void Start () {
		transform.localPosition = Vector3.zero;
        tileMgr = GameObject.Find("TileManager").GetComponent<TileMgr>();
	}
	
	// Update is called once per frame
	void Update () {

	    if (tileMgr.isLoad) {
            transform.localPosition = new Vector3(5000, 5000, 0);
            gameObject.SetActive(false);
	        enabled = false;
	    }
		
	}
}
