﻿using UnityEngine;

public class StageSoundBt : MonoBehaviour {
    private TweenRotation bt_x_rotation;
    private UIButton btButton;

    private AudioSource playerSound = null;

    public GameObject ButtonSpriteX;

    private DataMgr dataMgr;
    public bool isMute;

    // Use this for initialization
    private void Start() {
        dataMgr = GameObject.Find("DataManager").GetComponent<DataMgr>();
        GameObject player = GameObject.Find("player");

        if (player != null) {
            playerSound = player.GetComponent<AudioSource>();

        }

        isMute = dataMgr.dataClass.CURRENT_MUTE;
        bt_x_rotation = ButtonSpriteX.GetComponent<TweenRotation>();
        btButton = GetComponent<UIButton>();

        if (isMute) {
            bt_x_rotation.from = new Vector3(0, 0, 45);
            bt_x_rotation.to = new Vector3(0, 0, 45);
            bt_x_rotation.Reset();
        }
    }

    // Update is called once per frame
    private void OnClick() {
        isMute = !isMute;

        bt_x_rotation.enabled = true;

        dataMgr.dataClass.CURRENT_MUTE = isMute;
        dataMgr.SaveData();

        if (isMute) {
            bt_x_rotation.from = Vector3.zero;
            bt_x_rotation.to = new Vector3(0, 0, 45);
            NGUITools.soundVolume = 0;
            if(playerSound != null)
                playerSound.mute = true;
        }
        else {
            bt_x_rotation.from = new Vector3(0, 0, 45);
            bt_x_rotation.to = Vector3.zero;
            NGUITools.soundVolume = 1;
            if (playerSound != null)
                playerSound.mute = false;
        }

        bt_x_rotation.Reset();
    }
}