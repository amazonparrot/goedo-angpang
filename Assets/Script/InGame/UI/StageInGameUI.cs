﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageInGameUI : MonoBehaviour {

    public GameObject EpisodeNameLabel;
    private DataMgr dataMgr;

	// Use this for initialization
	void Start () {
        dataMgr = GameObject.FindWithTag("Data").GetComponent<DataMgr>();
        EpisodeNameLabel.GetComponent<UILabel>().text = "Stage - " + ((dataMgr.dataClass.CURRENT_EPISODE_ID * 15) + dataMgr.dataClass.CURRENT_STAGE_ID + 1);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
