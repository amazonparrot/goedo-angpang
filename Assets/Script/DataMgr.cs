﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class DataClass {

    public int CURRENT_STAGE_ID;
    public int CURRENT_EPISODE_ID;

    public int CURRENT_SKIN_SORT_INDEX;
    public int CURRENT_PLAYER_SKIN_SORT_INDEX;  //플레이어가 입고있는 옷 카테고리 인덱스
    public string CURRENT_PLAYER_SKIN;          //옷장씬의 옷 카테고리 인덱스

    //stage clear data
    public int[] STAGE_DATA;
    public StageInfo[] EPISODE_DATA;
    public int EPISODE_SIZE;

    public bool CURRENT_NOT_TUTORIAL;
    public bool CURRENT_FINAL;

    public bool CURRENT_MUTE;
    

    [SerializeField] public List<string> SKIN_UNLOCKED_LIST;
    [SerializeField] public List<string> CUPON_USED_LIST;

    public int CURRENT_LOTTO_TICKET;    //보안 필요
    public float CURRENT_LOTTO_HALF_TICKET;   //보안 필요
    public float CURRENT_LOTTO_AD_TIME;
    public float CURRENT_LOTTO_AD_COUNT;

    public bool CURRENT_ATZ;

    public string VERSION;

    public int CURRENT_LOTTO_SPECIAL_TICKET;
}

[System.Serializable]
public class DataClass2 {
    public bool CURRENT_ATZ;

    public string VERSION;

}

[System.Serializable]
public class DataClass3 {
    public string REMOVE_ADS;

    public string VERSION;

    public List<string> TICKETS_HISTORY;

}

[System.Serializable]
public class StageInfo {

    public int STAGE_SIZE;
    public int[] STAGE_SCORE;
    public bool[] STAGE_UNLOCK; //보안 필요

    public int STAGE_REWARD;

    public bool CURRENT_NOT_TUTORIAL;
    public bool CURRENT_FINAL;

}


public class DataMgr : MonoBehaviour {

    public DataClass dataClass;
    public DataClass2 dataClass2;
    public DataClass3 dataClass3;
    public Texture2D playerTexture;
    public Mesh playerMesh;

    public GameObject LoadingPopUp;
    private GameObject RealLoadingPopUp;

    public int[] StageSize;

    public string AndroidGameId = "1555725";

    private DisableSystemUI systemUI = null;
    private ClosetList closetList;

    void Awake() {

        DontDestroyOnLoad(this);

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        if (!LoadData()) {
            dataClass = new DataClass();

            dataClass.CURRENT_STAGE_ID = 0;
            dataClass.CURRENT_EPISODE_ID = 0;
            dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX = 0;
            dataClass.CURRENT_SKIN_SORT_INDEX = 0;
            dataClass.CURRENT_PLAYER_SKIN = "00_pc_map";
            dataClass.CURRENT_LOTTO_TICKET = 1;
            dataClass.CURRENT_LOTTO_HALF_TICKET = 0;
            dataClass.CURRENT_LOTTO_AD_TIME = 0;
            dataClass.CURRENT_LOTTO_AD_COUNT = 0;
            dataClass.CURRENT_LOTTO_SPECIAL_TICKET = 0;

            dataClass.CURRENT_NOT_TUTORIAL = false;
            dataClass.CURRENT_FINAL = false;
            dataClass.CURRENT_MUTE = false;

            dataClass.CURRENT_ATZ = false;
            dataClass.VERSION = Application.version;
            

            dataClass.STAGE_DATA = new int[3];
            for (int i = 0; i < 3; i++) {
                dataClass.STAGE_DATA[i] = 0;
            }

            dataClass.EPISODE_SIZE = StageSize.Length;
            //에피소드가 늘어나면 배열의 크기를 재배치 해야하는 예외처리를 꼭 해야함
            dataClass.EPISODE_DATA = new StageInfo[dataClass.EPISODE_SIZE];
            for (int i = 0; i < dataClass.EPISODE_DATA.Length; i++) {
                StageInfo tmp = new StageInfo();
                dataClass.EPISODE_DATA[i] = tmp;

                tmp.STAGE_SIZE = StageSize[i];

                tmp.CURRENT_NOT_TUTORIAL = false;
                tmp.CURRENT_FINAL = false;
                tmp.STAGE_SCORE = new int[tmp.STAGE_SIZE];
                tmp.STAGE_UNLOCK = new bool[tmp.STAGE_SIZE];
                tmp.STAGE_REWARD = 0;

                if (i == 0) {
                    tmp.STAGE_UNLOCK[0] = true;
                }

                


            }

            dataClass.SKIN_UNLOCKED_LIST = new List<string>();
            dataClass.SKIN_UNLOCKED_LIST.Add("00_pc_map");
            dataClass.CUPON_USED_LIST = new List<string>();

            dataClass.VERSION = Application.version;


            if (GetVersion(dataClass.VERSION) >= GetVersion("0.4.6")) {
                dataClass.CURRENT_SKIN_SORT_INDEX = 1;
            }


            //save
            SaveData();
        }

        if (!LoadData2()) {
            dataClass2 = new DataClass2();

            dataClass2.CURRENT_ATZ = dataClass.CURRENT_ATZ;
            dataClass.CUPON_USED_LIST = new List<string>();

            dataClass2.VERSION = Application.version;


            //save
            SaveData2();

        }

        if (!LoadData3()) {
            dataClass3 = new DataClass3();

            if (dataClass2.CURRENT_ATZ) {
                dataClass3.REMOVE_ADS = "AVA";
            }
            else {
                dataClass3.REMOVE_ADS = "";
            }

            dataClass3.TICKETS_HISTORY = new List<string>();

            dataClass3.VERSION = Application.version;
            dataClass2.VERSION = Application.version;
            dataClass.VERSION = Application.version;


            

            SaveData3();
            SaveData2();
            SaveData();

        }

        dataClass.EPISODE_DATA[0].STAGE_UNLOCK[0] = true;

        //할로윈 버전 갱신
        if (GetVersion(dataClass.VERSION) < GetVersion("0.4.6")) {
            dataClass.CURRENT_SKIN_SORT_INDEX = 1;
            dataClass.VERSION = Application.version;

            int prevClosetIndex = dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX;

            if (prevClosetIndex == -1) {
                dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX = 0;

            }else {
                dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX += 2;

            }
        }

        //스페셜 확정 티켓 버전 갱신
        if (GetVersion(dataClass.VERSION) < GetVersion("0.4.7")) {
            dataClass.CURRENT_LOTTO_SPECIAL_TICKET = 0;
            dataClass.VERSION = Application.version;
        }

        //뽑기권 구매 버전 갱신
        if (dataClass3.TICKETS_HISTORY == null || 
            GetVersion(dataClass3.VERSION) < GetVersion("0.4.7")) {

            dataClass3.TICKETS_HISTORY = new List<string>();
            dataClass3.VERSION = Application.version;

        }

        closetList = GetComponent<ClosetList>();
        SetPlayerSkin();
        
    }

	// Use this for initialization
	void Start () {
        systemUI = new DisableSystemUI();
        systemUI.DisableNavUI();

        StartCoroutine(SystemUIRun());

    }

    private IEnumerator SystemUIRun() {
        while (true) {
            yield return new WaitForSeconds(0.1f);

#if UNITY_ANDROID
            systemUI.Run();
#endif
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnApplicationFocus() {
        if (systemUI != null) {
#if UNITY_ANDROID
            systemUI.DisableNavUI();
            systemUI.Run();
#endif
        }
    }
    
    public void SaveData() {
        BinaryFormatter _binary_formatter = new BinaryFormatter();
        FileStream _filestream = File.Create(Application.persistentDataPath + "/Data.dat");

        _binary_formatter.Serialize(_filestream, dataClass);

        _filestream.Close();
        
        GetComponent<BGMMgr>().SetMute();
    }

    public void SaveData2() {
        BinaryFormatter _binary_formatter = new BinaryFormatter();
        FileStream _filestream = File.Create(Application.persistentDataPath + "/Data2.dat");

        _binary_formatter.Serialize(_filestream, dataClass2);

        _filestream.Close();
        
    }

    public void SaveData3() {
        BinaryFormatter _binary_formatter = new BinaryFormatter();
        FileStream _filestream = File.Create(Application.persistentDataPath + "/Data3.dat");

        _binary_formatter.Serialize(_filestream, dataClass3);

        _filestream.Close();

    }

    public bool LoadData() {
        bool _fhile_check = File.Exists(pathForDocumentsFile(Application.persistentDataPath + "/Data.dat"));

        if (!_fhile_check) {
            return false;
        }
        BinaryFormatter _binary_formatter = new BinaryFormatter();

        
        FileStream _filestream = File.Open((Application.persistentDataPath + "/Data.dat"), FileMode.Open);

        
        dataClass = (DataClass)_binary_formatter.Deserialize(_filestream);

        _filestream.Close();

        if (StageSize.Length > dataClass.EPISODE_SIZE) {
            int prevSize = dataClass.EPISODE_SIZE;

            dataClass.EPISODE_DATA = (StageInfo[]) ResizeArray(dataClass.EPISODE_DATA, StageSize.Length);
            dataClass.EPISODE_SIZE = StageSize.Length;

            for (int i = prevSize; i < StageSize.Length; i++) {
                StageInfo tmp = new StageInfo();
                dataClass.EPISODE_DATA[i] = tmp;

                tmp.STAGE_SIZE = StageSize[i];

                tmp.CURRENT_NOT_TUTORIAL = false;
                tmp.CURRENT_FINAL = false;
                tmp.STAGE_SCORE = new int[tmp.STAGE_SIZE];
                tmp.STAGE_UNLOCK = new bool[tmp.STAGE_SIZE];
                tmp.STAGE_REWARD = 0;
            }

        }

        return true;
    }

    public bool LoadData2() {
        bool _fhile_check = File.Exists(pathForDocumentsFile(Application.persistentDataPath + "/Data2.dat"));

        if (!_fhile_check) {
            return false;
        }
        BinaryFormatter _binary_formatter = new BinaryFormatter();


        FileStream _filestream = File.Open((Application.persistentDataPath + "/Data2.dat"), FileMode.Open);


        dataClass2 = (DataClass2)_binary_formatter.Deserialize(_filestream);

        _filestream.Close();

        return true;
    }

    public bool LoadData3() {
        bool _fhile_check = File.Exists(pathForDocumentsFile(Application.persistentDataPath + "/Data3.dat"));

        if (!_fhile_check) {
            return false;
        }
        BinaryFormatter _binary_formatter = new BinaryFormatter();


        FileStream _filestream = File.Open((Application.persistentDataPath + "/Data3.dat"), FileMode.Open);


        dataClass3 = (DataClass3)_binary_formatter.Deserialize(_filestream);

        _filestream.Close();

        return true;
    }

    public static string pathForDocumentsFile(string filename) {
        if (Application.platform == RuntimePlatform.IPhonePlayer) {
            string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(Path.Combine(path, "Documents"), filename);
        }

        else if (Application.platform == RuntimePlatform.Android) {
            string path = Application.persistentDataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path, filename);
        }

        else {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path, filename);
        }
    }

    public void SetLoading(bool active) {
        Destroy(RealLoadingPopUp);

        if (active) {
            RealLoadingPopUp = Instantiate(LoadingPopUp, GameObject.Find("Anchor").transform);
        }
    }

    public static IEnumerator SetScene(string SceneName) {
        GameObject.FindWithTag("Data").GetComponent<DataMgr>().SetLoading(true);
        SceneManager.LoadSceneAsync(SceneName);
        yield return null;
    }

    private void SetPlayerSkin() {


        playerTexture =
    Resources.Load<Texture2D>("pcskin/" + closetList.ClosetCategoryPath[dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX] + "/map/" +
                              dataClass.CURRENT_PLAYER_SKIN);
        playerMesh =
            Resources.Load<Mesh>("pcskin/" + closetList.ClosetCategoryPath[dataClass.CURRENT_PLAYER_SKIN_SORT_INDEX] + "/fbx/" +
                                 playerTexture.name);
    }

    public static System.Array ResizeArray(System.Array oldArray, int newSize) {
        int oldSize = oldArray.Length;
        System.Type elementType = oldArray.GetType().GetElementType();
        System.Array newArray = System.Array.CreateInstance(elementType, newSize);
        int preserveLength = System.Math.Min(oldSize, newSize);
        if (preserveLength > 0)
            System.Array.Copy(oldArray, newArray, preserveLength);
        return newArray;
    }

    private void OnLevelWasLoaded(int level) {
        
        SetUIRootScaleStyle();

    }

    public static void SetUIRootScaleStyle() {
        GameObject rootObj = GameObject.Find("UI Root (2D)");

        if (rootObj == null) return;

        UIRoot _root = rootObj.GetComponent<UIRoot>();

        if (Application.platform == RuntimePlatform.WindowsPlayer) {
            _root.scalingStyle = UIRoot.Scaling.FixedSize;
        }
        else if (_root.scalingStyle != UIRoot.Scaling.FixedSizeOnMobiles) {
            _root.scalingStyle = UIRoot.Scaling.FixedSizeOnMobiles;
        }
    }

    public static float GetVersion(string version) {

        version = version.Replace("f", ".");

        string[] sources = version.Split('.');

        float result = -1;

        result = int.Parse(sources[0]) * 10000;
        result += int.Parse(sources[1]) * 100;
        result += int.Parse(sources[2]);

        if (sources.Length > 3) {
            result += int.Parse(sources[3])/100f;
        }

        return result;

    }
}
