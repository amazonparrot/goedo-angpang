﻿using UnityEngine;

public class BGMMgr : MonoBehaviour {
    private AudioSource _audioSource;
    public AudioClip ClosetSceneAudioClip;

    public bool isMute;

    public AudioClip MainSceneAudioClip;
    public AudioClip Stage1AudioClip;
    public AudioClip Stage2AudioClip;
    public AudioClip Stage3AudioClip;
    public AudioClip Episode2AudioClip;
    public AudioClip Episode3AudioClip;

    public AudioClip HalloweenAudioClip;

    public AudioClip Ep1CartoonAudioClip;
    public AudioClip Ep2CartoonAudioClip;
    public AudioClip Ep3CartoonAudioClip;

    public AudioClip Ep1CartoonEndAudioClip;
    public AudioClip Ep2CartoonEndAudioClip;
    public AudioClip Ep3CartoonEndAudioClip;


    // Use this for initialization
    private void Start() {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = MainSceneAudioClip;
        _audioSource.Stop();

        SetMute();
    }

    // Update is called once per frame
    private void Update() {
    }

    private void OnLevelWasLoaded(int level) {

        if (_audioSource != null) {
            _audioSource.pitch = 1;
            _audioSource.volume = 1;

            //level--;

            if (level == 3) {
                _audioSource.clip = ClosetSceneAudioClip;
                _audioSource.Play(0);
            }
            else if (level == 2) {
                switch (GetComponent<DataMgr>().dataClass.CURRENT_EPISODE_ID) {
                    case 0: {
                        var stageId = ResourceManager.SetStageId(GetComponent<DataMgr>().dataClass.CURRENT_STAGE_ID + 1);

                        switch (stageId) {
                            case 3:
                                _audioSource.clip = Stage3AudioClip;
                                _audioSource.Play(0);
                                break;
                            case 2:
                                _audioSource.clip = Stage2AudioClip;
                                _audioSource.Play(0);
                                break;
                            default:
                                _audioSource.clip = Stage1AudioClip;
                                _audioSource.Play(0);
                                break;
                        }
                        break;
                    }

                    case 1:
                        _audioSource.clip = Episode2AudioClip;
                        _audioSource.Play(0);
                        break;

                    case 2:
                        _audioSource.clip = Episode3AudioClip;
                        _audioSource.Play(0);
                        break;

                    default:
                        _audioSource.clip = null;
                        _audioSource.Stop();
                        break;
                }
            }
            else if (level == 4) {
                int episodeId = GetComponent<DataMgr>().dataClass.CURRENT_EPISODE_ID;

                switch (episodeId) {
                    case 0:
                        _audioSource.clip = GetComponent<DataMgr>().dataClass.CURRENT_STAGE_ID == 0 ? Ep1CartoonAudioClip : Ep1CartoonEndAudioClip;
                        break;
                    case 1:
                        _audioSource.clip = GetComponent<DataMgr>().dataClass.CURRENT_STAGE_ID == 0 ? Ep2CartoonAudioClip : Ep2CartoonEndAudioClip;
                        break;
                    case 2:
                        _audioSource.clip = GetComponent<DataMgr>().dataClass.CURRENT_STAGE_ID == 0 ? Ep3CartoonAudioClip : Ep3CartoonEndAudioClip;
                        break;

                }
                _audioSource.Play(0);
            }
            else if (level == 1) {
                if (_audioSource.clip == MainSceneAudioClip) return;
                _audioSource.clip = MainSceneAudioClip;
                _audioSource.Play(0);
            }
            else if (level == 5) {
                _audioSource.clip = HalloweenAudioClip;
                _audioSource.Play(0);
            }
        }
    }

    public void ForcePlay() {
        _audioSource.Play(0);
    }

    public void Stop() {
        _audioSource.Stop();
    }

    public void SetMute() {
        isMute = GetComponent<DataMgr>().dataClass.CURRENT_MUTE;
        if (_audioSource != null) _audioSource.mute = isMute;
    }

    public void SetVolume(float vol) {
        _audioSource.volume = vol;
    }
}