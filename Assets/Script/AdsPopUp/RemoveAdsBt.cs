﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveAdsBt : MonoBehaviour {

    public GameObject CodeLabel;
    private UILabel label;

    private RemoveAdIAP removeAdIAP;
    private string RemoveAdsPurchaseId = "product_removeads_id_01";


    // Use this for initialization
    void Start () {
	    removeAdIAP = GetComponent<RemoveAdIAP>();
        label = CodeLabel.GetComponent<UILabel>();
    }
	
	// Update is called once per frame
	void Update () {
	    if (removeAdIAP.isProcessed) {
	        label.text = "결제 코드 : " + removeAdIAP.GetIAPCode();
	    }
	    else if(removeAdIAP.GetIAPCode() != ""){
            label.text = "에러 : " + removeAdIAP.GetIAPCode();
        }
	}

    void OnClick() {
        if(!removeAdIAP.isProcessed)
            removeAdIAP.purchase(RemoveAdsPurchaseId);
    }
}
