﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopCloseBt : MonoBehaviour {

    public GameObject panel;

    private UIPanel _uiPanel;
    private bool isClose;

	// Use this for initialization
	void Start () {
	    _uiPanel = panel.GetComponent<UIPanel>();
	    isClose = false;
	}
	
	// Update is called once per frame
	void Update () {

	    if (isClose) {

            if (_uiPanel.alpha > 0.01f)
                _uiPanel.alpha = Mathf.Lerp(_uiPanel.alpha, 0, 0.1f);
            else {
                _uiPanel.alpha = 0;
                Destroy(panel);
            }
        }

	}

    void OnClick() {
        isClose = true;
    }


}
