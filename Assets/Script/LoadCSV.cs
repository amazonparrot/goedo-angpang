﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


public class LoadCSV : MonoBehaviour {

    public string path;
    public string[] sources;
    public DataTable table;

    public bool isLoaded = false;
    public bool isSuccess = false;

    public bool LoadStreamingAssetsWhenStart;

    // Use this for initialization
    void Start () {
        if (LoadStreamingAssetsWhenStart) {
            LoadToStreamingAssets(path);
        }
    }
	
	// Update is called once per frame
	void Update () {

	}


    public void Load(string path) {
        StartCoroutine(LoadFile(path));
    }

    public void LoadToStreamingAssets(string path) {

        string finalPath;

        if (Application.platform == RuntimePlatform.Android) {
            finalPath = "jar:file://" + Application.dataPath + "!/assets" + "/" + path;
        }
        else {
            finalPath = "file://" + Application.streamingAssetsPath + "/" + path;
        }

        Load(finalPath);

    }

    private IEnumerator LoadFile(string path) {
        if (path.Length == 0) yield return null;

        WWW File = new WWW(path);
        yield return File;

        byte[] bConvert = Encoding.Convert(Encoding.UTF8, Encoding.Default, File.bytes);

        // utf-8 인코딩
        string encodedString = System.Text.Encoding.Default.GetString(bConvert);

        string[] sources_line = encodedString.Split('\n');


        if (sources_line.Length == 0) {
            yield return File;
        }

        List<string> sourcesList = new List<string>();
        List<int> sourcesCountList = new List<int>();
        int sourcesMaximumCount = 0;

        for (int line = 0; line < sources_line.Length; line++) {

            string source_line = sources_line[line];
            string[] sources_comma = source_line.Split(',');

            bool textMode = false;
            int sourcesCount = 0;

            for (int comma = 0; comma < sources_comma.Length; comma++) {

                string source_comma = sources_comma[comma];

                if (textMode) {
                    source_comma = "," + source_comma;
                }

                if (string.IsNullOrEmpty(source_comma)) {
                    sourcesList.Add(null);

                    sourcesCount++;
                    continue;
                }

                if (source_comma.TrimStart().StartsWith("\"")) {
                    textMode = true;
                    source_comma = source_comma.Substring(1);
                    source_comma = source_comma.Replace("\"\"", "\"");
                    sourcesList.Add(source_comma);

                    sourcesCount++;
                    continue;
                }

                if (source_comma.TrimEnd().EndsWith("\"")) {
                    textMode = false;
                    source_comma = source_comma.Substring(0, source_comma.Length - 2);
                    source_comma = source_comma.Replace("\"\"", "\"");
                    sourcesList[sourcesList.Count - 1] += source_comma;
                    continue;
                }

                source_comma = source_comma.Replace("\"\"", "\"");

                if (textMode) {

                    sourcesList[sourcesList.Count - 1] += source_comma;

                }
                else {
                    
                    sourcesList.Add(source_comma);

                    sourcesCount++;

                }


            }

            sourcesMaximumCount = (sourcesCount > sourcesMaximumCount) ? 
                sourcesCount : sourcesMaximumCount;

            sourcesCountList.Add(sourcesCount);

        }

        int index = 0;
        table = new DataTable(sourcesCountList.Count, sourcesMaximumCount);

        for (int i = 0; i < sourcesCountList.Count; i++) {

            int size = sourcesCountList[i];

            for (int j = 0; j < size; j++) {
                table.Insert(sourcesList[index + j], j, i);
            }

            index += size;
        }

        sources = sourcesList.ToArray();

        isLoaded = true;
        isSuccess = sources.Length > 0;

    }
}
